//
//  NCArtworkCell.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 11/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NCArtwork;

@interface NCArtworkCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) NCArtwork * artwork;

@end
