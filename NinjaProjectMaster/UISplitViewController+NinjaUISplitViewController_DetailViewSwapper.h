//
//  UISplitViewController+NinjaUISplitViewController_DetailViewSwapper.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISplitViewController (DetailViewSwapper)

- (void)prepareReplaceSegueFor: (UINavigationController *)detailNavViewController;

- (void)dismissMasterPopoverFrom: (UIViewController *)detailViewController;



@end
