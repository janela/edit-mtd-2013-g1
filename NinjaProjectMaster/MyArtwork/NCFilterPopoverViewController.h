//
//  FilterPopoverViewController.h
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 31/05/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCCriarObraViewController.h"


@interface NCFilterPopoverViewController : UIViewController

@property (nonatomic,strong) NCCriarObraViewController * delegate;

- (IBAction)buttonFilter1:(id)sender;

- (IBAction)buttonFilter2:(id)sender;


@end
