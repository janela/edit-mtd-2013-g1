//
//  NCMapViewController.m
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 17/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCMapViewController.h"
#import "NCArtworkDetailViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"

@interface NCMapViewController ()

@end

@implementation NCMapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
        
    
    
    
    
    
}

-(void) viewWillAppear:(BOOL)animated{
    
    
    // 1
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = 38.575816;
    zoomLocation.longitude= -9.195655;
    
    // 2
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 600000, 600000);
    
    // 3
    [_map setRegion:viewRegion animated:YES];

    
    
    
    [self.map addAnnotations:self.artworkCollection];
    
    


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(NCArtwork <MKAnnotation>*)annotation {
    
    
    static NSString *identifier = @"MyLocation";
        
    MKAnnotationView *annotationView = (MKAnnotationView *) [_map dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"pushpin.png"];
        UIButton * button = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.rightCalloutAccessoryView = button;

        
        
    
    } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    
    
    return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil ];
    NCArtwork * artwork = (NCArtwork*) view.annotation;
    NCArtworkDetailViewController * detailView = (NCArtworkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ShowArtworkDetailView"];

    
    [detailView setArtwork:artwork];
    
    [self.navigationController pushViewController:detailView animated:YES];
    
}


- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

@end
