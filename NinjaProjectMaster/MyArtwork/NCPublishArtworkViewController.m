//
//  NCPublishArtworkViewController.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 19/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCPublishArtworkViewController.h"
#import "NCArtwork.h"
#import <CoreLocation/CoreLocation.h>
#import "NCArtworkManager.h"
#import "NCCategoryViewController.h"

@interface NCPublishArtworkViewController ()
{
    
    CLLocationManager * locationManager;
    
    UIPopoverController * categoryPopover;
    
   
    
    
}
@end

@implementation NCPublishArtworkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDSCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    locationManager =  [[CLLocationManager alloc] init];
    
    [locationManager startUpdatingLocation];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}

- (IBAction)buttonPublishArtwork:(id)sender {
    
    NSMutableArray * arrayComTagsId = [[NSMutableArray alloc]init];
    
    for (int i = 1; i <=10; i++) {
        
        UIButton * button = (UIButton*)[self.view viewWithTag:i];
        
        if(button.selected){
            
            [arrayComTagsId addObject:[NSString stringWithFormat:@"%i",i]];
            
            
        }
        
    }

    
    
    NSString * idstring = [[arrayComTagsId valueForKey:@"description"] componentsJoinedByString:@","];
    
    [[NCArtworkManager sharedManager] publishArtWorkWithTitle:self.textfieldTitle.text withDescription:self.textfieldDescription.text withTags:idstring withLocation:[self getUserLocation] withImage:self.image withCompletionBlock:^(NSError * error, NSDictionary* dic){
    
        
        
        [self.navigationController popToRootViewControllerAnimated:NO];
    
    }];
    
    
}
- (IBAction)buttonCancelPublication:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(CLLocationCoordinate2D) getUserLocation{
    
        
    return locationManager.location.coordinate;
    
}

- (IBAction)selectTags:(id)sender {
    
    UIButton * button = (UIButton*)sender;
    
    if(!button.selected){
        
        [button setSelected:YES];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        
    }
    else{
        
        [button setSelected:NO];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        
        
        
    }
    

    
    
    
}
@end
