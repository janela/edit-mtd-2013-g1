//
//  Filtergram.h
//  Filtros
//
//  Created by Rui Bordadágua on 09/05/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface NCImageEditor : NSObject

-(UIImage*) aplicarFiltroSepia;

-(UIImage*) aplicarFiltroBlur;

-(id) initWithImage: (UIImage*) image;

-(UIImage*) applyFilter:(int) filternumber;

-(UIImage *) applyUndo;

-(UIImage *) getVisualEffectWithNumber:(int) visualEffectNumber;

-(UIImage *) applyVisualEffect:(UIImageView*) imageViewThatContainsVisualEffect inImageView: (UIImageView*) imageViewThatContainsImage ;





@end
