//
//  APPViewController.m
//  CameraApp
//
//  Created by Rafael Garcia Leiva on 10/04/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import "APPViewController.h"
#import "NCCriarObraViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface APPViewController ()
{
    UIPopoverController * popover;
    
    UIImagePickerController * picker;

}
@end

@implementation APPViewController
-(void)viewWillAppear:(BOOL)animated{
    
    [self.usePhoto setHidden:YES];
        
}


- (void)viewDidLoad {
    
    [super viewDidLoad];
    
     
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Device has no camera"
                                                        delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    
   
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}

- (IBAction)takePhoto:(UIButton *)sender {
    
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    
        
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (IBAction)selectPhoto:(UIButton *)sender {
    
    picker = [[UIImagePickerController alloc]init];
    
    picker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    NSLog(@"Media disponivel %@", [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]);
    
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    
    popover = [[UIPopoverController alloc] initWithContentViewController:picker];
    
    [popover presentPopoverFromRect:CGRectMake(400, 400, 500, 500) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionAny animated:YES];
    
    
}


- (IBAction)actionGoToCreateArtWork:(id)sender {
    
    
    
    
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)pickercontroller didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    UIImage * chosenImage =  [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    CGSize cgsize = CGSizeMake(640, 480);
    
    NSLog(@"w: %f, h:%f", chosenImage.size.width, chosenImage.size.height);
    
    UIImage * imageScaled = [self imageWithImage:chosenImage scaledToSize:cgsize];
    
    self.imageView.image = imageScaled;
    
    

    [pickercontroller dismissViewControllerAnimated:YES completion:NULL];
    
    [self.usePhoto setHidden:NO];
    
    
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController *)pickercontroller {

    [pickercontroller dismissViewControllerAnimated:YES completion:NULL];
    
}

#pragma mark - Segue Method
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"ShowCreateArtwork"])
    {
        // Get reference to the destination view controller
        NCCriarObraViewController * advc = [segue destinationViewController];
        
        [advc setInitialImage:self.imageView.image];
        
    }
}

- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}


@end
