//
//  NCMapViewController.h
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 17/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCArtwork.h"
#import "NCArtworkManager.h"

@interface NCMapViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) IBOutlet MKMapView *map;

@property (strong, nonatomic) NSArray * artworkCollection;

- (IBAction)LogoutButtonPressed:(id)sender;

@end
