//
//  NCMyArtworkCell.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 16/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCArtwork.h"

@interface NCMyArtworkCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property NCArtwork * artwork;

@end
