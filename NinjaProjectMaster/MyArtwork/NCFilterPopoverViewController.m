//
//  FilterPopoverViewController.m
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 31/05/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import "NCFilterPopoverViewController.h"

@interface NCFilterPopoverViewController ()

@end

@implementation NCFilterPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonFilter1:(id)sender {
    
    [self.delegate filterChoosed:1];
    
}

- (IBAction)buttonFilter2:(id)sender {
    
    [self.delegate filterChoosed:2];
}

@end
