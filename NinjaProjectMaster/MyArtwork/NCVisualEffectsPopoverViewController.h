//
//  NCVisualEffectsPopoverViewController.h
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 01/06/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCCriarObraViewController.h"

@interface NCVisualEffectsPopoverViewController : UIViewController

@property (nonatomic,strong) NCCriarObraViewController * delegate;

- (IBAction)visualEffectKaboomAction:(id)sender;
@end
