//
//  CriarObraViewController.h
//  Ninja App Simplex 2
//
//  Created by EDIT Guest on 29/04/2013.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NinjaBaseDetailViewController.h"

@interface NCCriarObraViewController : NinjaBaseDetailViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

- (IBAction)pickAPhoto:(id)sender;

- (IBAction)buttonPublish:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView * image;

@property (weak, nonatomic) IBOutlet UIButton *buttonPickPhoto;

@property (weak, nonatomic) IBOutlet UIButton *buttonUndoPhotoEdition;

@property (weak, nonatomic) IBOutlet UIButton *buttonClickVisualEffect;

@property (weak, nonatomic) IBOutlet UIButton *buttonApplyVisualEffect;

@property (weak, nonatomic) IBOutlet UIButton *buttonCancelVisualEffect;

@property (weak, nonatomic) IBOutlet UIButton *buttonFilters;

@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property UIImage * initialImage;

- (IBAction)buttonApplyVisualEffect:(id)sender;

- (IBAction)buttonCancelVisualEffect:(id)sender;

- (IBAction)buttonFilterPopover:(id)sender;
    
- (IBAction)buttonUndoAction:(id)sender;

- (IBAction)buttonVisualEffectsPopover:(id)sender;
    
-(void) filterChoosed: (int) filternumber;

-(void) visualEffectchoosed:(int) visualeffectNumber;

- (IBAction)buttonSaveArtwork:(id)sender;

- (IBAction)buttonCancelArtworkCreation:(id)sender;

- (IBAction)LogoutButtonPressed:(id)sender;

@end
