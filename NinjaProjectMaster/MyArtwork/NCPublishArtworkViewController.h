//
//  NCPublishArtworkViewController.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 19/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCCategoryPopoverDelegate.h"

@interface NCPublishArtworkViewController : UIViewController 

@property (weak, nonatomic) IBOutlet UITextField *textfieldTitle;

@property (weak, nonatomic) IBOutlet UITextField *textfieldDescription;


- (IBAction)buttonPublishArtwork:(id)sender;

- (IBAction)buttonCancelPublication:(id)sender;

@property (weak, nonatomic) UIImage * image;

- (IBAction)selectTags:(id)sender;

@end
