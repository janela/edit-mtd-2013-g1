//
//  CriarObraViewController.m
//  Ninja App Simplex 2
//
//  Created by EDIT Guest on 29/04/2013.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import "NCCriarObraViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "NCFilterPopoverViewController.h"
#import "NCImageEditor.h"
#import "NCVisualEffectsPopoverViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCPublishArtworkViewController.h"

#import "NCArtworkManager.h"

@interface NCCriarObraViewController ()
{
    
    UIImagePickerController * picker;
    
    UIPopoverController * popover;
    
    UIPopoverController * popoverVisualEffects;
    
    UIPopoverController * popoverfilters;
    
    NCFilterPopoverViewController * filterViewContoller;
    
    NCImageEditor * imageEditor;
    
    UIImageView * visualEffect;
    
    CGPoint visualEffectOrigin;
    
    NCVisualEffectsPopoverViewController * visualeffectpopover;
    
}



@end

@implementation NCCriarObraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    [self interfaceModoNormal];
    
    self.image.image = self.initialImage;
    
    imageEditor = [[NCImageEditor alloc]initWithImage:self.initialImage];
    
    
	
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}

- (IBAction)goHome:(id)sender {
}
- (IBAction)pickAPhoto:(id)sender {
    
    picker = [[UIImagePickerController alloc]init];
    
    picker.delegate = self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        
        [picker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    NSLog(@"Media disponivel %@", [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]);
    
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    
    popover = [[UIPopoverController alloc] initWithContentViewController:picker];
    
    [popover presentPopoverFromRect:CGRectMake(400, 400, 500, 500) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionAny animated:YES];
    
    
}

- (IBAction)buttonPublish:(id)sender {
    
    
    
    
    
}


    
    


#pragma mark ImagePicker Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)pickerM didFinishPickingMediaWithInfo:
    (NSDictionary *)info{
    
    NSLog(@"Informações %@",info);
    
    UIImage * image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    [_image setImage:image];
    
    imageEditor = [[NCImageEditor alloc] initWithImage:image];
    
    [popover dismissPopoverAnimated:YES];
    
    
    
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [popover dismissPopoverAnimated:YES];
}

#pragma mark Interface Modes

-(void) interfaceModoNormal{
    
    [_buttonUndoPhotoEdition setHidden:NO];
    [_buttonApplyVisualEffect setHidden:YES];
    [_buttonCancelVisualEffect setHidden:YES];

    
}

-(void) interfaceModoAplicarEfeitoVisual{
    
    [_buttonUndoPhotoEdition setHidden:NO];
    [_buttonApplyVisualEffect setHidden:NO];
    [_buttonCancelVisualEffect setHidden:NO];

}

#pragma mark IBAction

- (IBAction)buttonApplyVisualEffect:(id)sender {
    
    UIImage * newimage = [imageEditor applyVisualEffect:visualEffect inImageView:self.image];
    [self changeCurrentImage:newimage];
    [visualEffect removeFromSuperview];
    [self interfaceModoNormal];
    
}

- (IBAction)buttonCancelVisualEffect:(id)sender {
    
    [visualEffect removeFromSuperview];
    [self interfaceModoNormal];
}

- (IBAction)buttonFilterPopover:(id)sender {
    
    if(filterViewContoller == nil){
        
        filterViewContoller = [[NCFilterPopoverViewController alloc]initWithNibName:@"NCFilterPopoverViewController" bundle:nil];
        [filterViewContoller setContentSizeForViewInPopover:CGSizeMake(250, 250)];
        
        filterViewContoller.delegate = self;
        
        
    }
    
    
   popoverfilters = [[UIPopoverController alloc] initWithContentViewController:filterViewContoller];
    
    [popoverfilters presentPopoverFromRect:CGRectMake(70, 565, 170, 60) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown  animated:YES];
    }

- (IBAction)buttonUndoAction:(id)sender {
    
    [self changeCurrentImage:[imageEditor applyUndo]];
    
    
}





- (IBAction)buttonVisualEffectsPopover:(id)sender {
    
    if(visualeffectpopover == nil){
        
        visualeffectpopover = [[NCVisualEffectsPopoverViewController alloc]initWithNibName:@"NCVisualEffectsPopoverViewController" bundle:nil];
        [visualeffectpopover setContentSizeForViewInPopover:CGSizeMake(250, 250)];
        
        visualeffectpopover.delegate = self;
        
        
    }
    
    
    popoverVisualEffects = [[UIPopoverController alloc] initWithContentViewController:visualeffectpopover];
    
    [popoverVisualEffects presentPopoverFromRect:CGRectMake(180, 565, 170, 60) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown  animated:YES];
}

-(void)filterChoosed:(int)filternumber{
    
    [popoverfilters dismissPopoverAnimated:NO];
    
    UIImage * newImage = [imageEditor applyFilter:filternumber];
   [self.image setImage:newImage];
}


-(void) changeCurrentImage:(UIImage*)image{
    
    
    [self.image setImage:image];
}


-(void) loadVisualEffectToView: (UIImage*) image{
    
    
    visualEffect = [[UIImageView alloc] initWithImage:image];
    
    CGSize size = { image.size.width,image.size.height};
    
    CGPoint ponto = self.image.center;
    
    CGRect rect = {  ponto, size};
    
    
    [visualEffect setFrame:rect];
    
    [visualEffect setUserInteractionEnabled:YES];
    [visualEffect setMultipleTouchEnabled:YES];
    
    NSLog(@"Está activo? %i",visualEffect.isUserInteractionEnabled);
    
    [self.view addSubview:visualEffect];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureActionLimitedToImageFrame:)];
    
    [visualEffect addGestureRecognizer:panGesture];
    
    [self interfaceModoAplicarEfeitoVisual];

}

- (IBAction)panGestureActionLimitedToImageFrame:(UIPanGestureRecognizer*)sender {
    
    CGPoint translation = [ sender translationInView:self.view];
    
    //Os limites são calculados segundo a frame
    float novopontoFrameX = sender.view.frame.origin.x + translation.x;
    
    float novopontoFrameY = sender.view.frame.origin.y + translation.y;
    
    float novopontoX = sender.view.center.x + translation.x;
    
    float novopontoY = sender.view.center.y+ translation.y;
    
    
    if( novopontoFrameX < self.image.frame.origin.x
       || novopontoFrameX + sender.view.frame.size.width > self.image.frame.origin.x + self.image.frame.size.width){
        
        novopontoX = sender.view.center.x;
        
        
        
    }
    
    if( novopontoFrameY < self.image.frame.origin.y || novopontoFrameY + visualEffect.frame.size.height > self.image.frame.origin.y + self.image.frame.size.height){
        
        novopontoY = sender.view.center.y;
        
        
        
    }
    
    
    
    sender.view.center=CGPointMake(novopontoX, novopontoY);
    
    //Reseting the translation.
    [sender setTranslation:CGPointMake(0, 0) inView:sender.view];
    
    
    
    
    if (sender.state == UIGestureRecognizerStateEnded){
        
        visualEffectOrigin = sender.view.frame.origin;
    }
    
    
    
    
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer{
    
    return YES;
}

-(void) visualEffectchoosed:(int) visualeffectNumber{
    
    [popoverVisualEffects dismissPopoverAnimated:YES];
    
    //Pergunta ao imageEditor qual é o efeito visual associado
    UIImage * visualeffect = [imageEditor getVisualEffectWithNumber:visualeffectNumber];
    
    [self loadVisualEffectToView:visualeffect];
}

- (IBAction)buttonSaveArtwork:(id)sender {
    
    [[NCArtworkManager sharedManager] saveArtwork:self.image.image];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}

- (IBAction)buttonCancelArtworkCreation:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
       
    
}


- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([[segue identifier] isEqual: @"ShowPublish"]){
        
        NCPublishArtworkViewController * pavc = [segue destinationViewController];
        
        [pavc setImage:self.image.image];
        
    }
    
    
}


-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    if([identifier isEqual: @"ShowPublish"]){
        
        
        
    }
    
}

@end
