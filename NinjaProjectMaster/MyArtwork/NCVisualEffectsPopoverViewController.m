//
//  NCVisualEffectsPopoverViewController.m
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 01/06/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import "NCVisualEffectsPopoverViewController.h"

@interface NCVisualEffectsPopoverViewController ()

@end

@implementation NCVisualEffectsPopoverViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)visualEffectKaboomAction:(id)sender {
    
    [self.delegate visualEffectchoosed:1 ];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
