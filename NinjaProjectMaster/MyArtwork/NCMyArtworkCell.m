//
//  NCMyArtworkCell.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 16/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCMyArtworkCell.h"

@implementation NCMyArtworkCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
