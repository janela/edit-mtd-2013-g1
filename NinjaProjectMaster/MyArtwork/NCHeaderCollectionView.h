//
//  NCHeaderCollectionView.h
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 19/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCHeaderCollectionView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIImageView *imageHeaderSection;

@end
