//
//  Filtergram.m
//  Filtros
//
//  Created by Rui Bordadágua on 09/05/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//



/**
 
 Processing Images
 
 Core Image has three classes that support image processing on iOS and OS X:
 
 CIFilter is a mutable object that represents an effect. A filter object has at least one input parameter and produces an output image.
 CIImage is an immutable object that represents an image. You can synthesize image data or provide it from a file or the output of another CIFilter object.
 CIContext is an object through which Core Image draws the results produced by a filter. A Core Image context can be based on the CPU or the GPU.
*/


#import "NCImageEditor.h"
#import <CoreImage/CoreImage.h>

@interface NCImageEditor()
{
    
    NSMutableArray * historicoImagens;
    
    CIContext * filterContext;
    
    UIImage * currentImage;
    
    NSDictionary * visualEffectsImagesPaths;
    
    CGContextRef  visualEffectContext;
    
}


@end


@implementation NCImageEditor



- (id)initWithImage: (UIImage*) image
{
    self = [super init];
    if (self) {
        
        //Best perfomance: Create only one CIContext. Context store a lot of information.
        
        filterContext = [CIContext contextWithOptions:nil];
        
        currentImage = image;
        
        historicoImagens = [[NSMutableArray alloc] initWithObjects:currentImage, nil];
        
        visualEffectsImagesPaths = [[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"Resources/Images/kaboom.png", nil];
        
        
    }
    return self;
}

-(UIImage*) applyFilter:(int) filternumber{
    
    UIImage * image;
    
    switch (filternumber) {
        case 1:
            image = [self aplicarFiltroSepia];
            break;
        case 2:
            image = [self aplicarFiltroBlur];
            break;
        default:
            break;
    }
    
    currentImage = image;
    
    [historicoImagens addObject:image];
    
    return image;
    
}




-(UIImage*) aplicarFiltroSepia{

    
    
    CIImage * image = [CIImage imageWithCGImage:currentImage.CGImage];
    
    
    //Create the filter and set values for its input parameters. There are more compact ways to set values than shown here. See “Creating a CIFilter Object and Setting Values.”

    
    CIFilter *filter = [CIFilter filterWithName:@"CISepiaTone"];
    
    
    //Não entendo este método.Será que estamos a dizer sobre que imagem o filtro vai trabalhar?
    [filter setValue:image forKey:kCIInputImageKey];
    
    [filter setValue:[NSNumber numberWithFloat:2.8f] forKey:@"InputIntensity"];
    
    
    //Get the output image. The output image is a recipe for how to produce the image. The image is not yet rendered. See “Getting the Output Image.”
    CIImage * result = [filter valueForKey:kCIOutputImageKey];
    
    
    
    
    CIFilter * vignette = [CIFilter filterWithName:@"CIVignette"];
    
    [vignette setDefaults];
    
    [vignette setValue:[NSNumber numberWithFloat:3.0f] forKey:@"inputIntensity"];
    
    [vignette setValue:[NSNumber numberWithFloat:2.0f] forKey:@"inputRadius"];
    
    [vignette setValue:result forKey:kCIInputImageKey];
    
    
    CIImage * resultSepiaVignette = [vignette valueForKey:kCIOutputImageKey];
    
    //Render the CIImage to a Core Graphics image that is ready for display or saving to a file.

    CGImageRef cgImage = [filterContext createCGImage:resultSepiaVignette fromRect:[resultSepiaVignette extent]];
                          
                          
    /**
    
    CGImageRef is an opaque type that encapsulates bitmap image information.
    
    A bitmap image (or sampled image) is an array of pixels (or samples). Each pixel represents a single point in the image. JPEG, TIFF, and PNG graphics files are examples of bitmap images.
     
     
    
    */

    UIImage * imagemASerApresentada = [[UIImage alloc] initWithCGImage:cgImage];
    
    
    return imagemASerApresentada;
    
    
}

-(UIImage*) aplicarFiltroBlur{
    
    
        
    CIImage * imagem = [CIImage imageWithCGImage:currentImage.CGImage
                        ];
    
    CIFilter *blurFilter = [CIFilter filterWithName:@"CIVibrance"];
    [blurFilter setDefaults];
    [blurFilter setValue: imagem forKey: @"inputImage"];
    [blurFilter setValue: [NSNumber numberWithFloat:10.0f]
                  forKey:@"inputAmount"];

    CIImage * result = [blurFilter valueForKey:kCIOutputImageKey];
    
    CGImageRef imagemRenderizada = [filterContext createCGImage:result fromRect:[result extent]];
    
    UIImage * imagemASerApresentada = [[UIImage alloc] initWithCGImage:imagemRenderizada];
    
    return imagemASerApresentada;
    
}

-(UIImage *) applyUndo
{
    if([historicoImagens count]>1){
        [historicoImagens removeLastObject];
    }
    
    currentImage = [historicoImagens lastObject];
    return currentImage;
    
    
    
}

-(UIImage *) getVisualEffectWithNumber:(int) visualEffectNumber{
    
    
    
    NSString * imagefile = [[NSBundle mainBundle]  pathForResource:@"kaboom" ofType:@"png"];
    
    NSLog(@"%@", imagefile);
    
    NSURL * path = [NSURL fileURLWithPath:imagefile];
        
    CIImage * imagemCI = [CIImage imageWithContentsOfURL:path];
    
    UIImage * imagem = [UIImage imageWithCIImage:imagemCI];
        
    return imagem;

    
}

-(UIImage *) applyVisualEffect:(UIImageView*) imageViewThatContainsVisualEffect inImageView: (UIImageView*) imageViewThatContainsImage{
    
    
    
    UIImage * imagemA = imageViewThatContainsImage.image;
    UIImage * imagemB = imageViewThatContainsVisualEffect.image;
    
    //Onde desenhar o efeito visual? Coordenada do efeito visual menos coordenada do view background
    
    
    NSLog(@"pontos ViewVE %f %f", imageViewThatContainsVisualEffect.frame.origin.x ,imageViewThatContainsVisualEffect.frame.origin.y);
    
    
    //NSLog(@"Centro frame %f %f", imageViewThatContainsImage.center.x, imageViewThatContainsImage.center.y);
    
    
    float x = imageViewThatContainsVisualEffect.frame.origin.x - imageViewThatContainsImage.frame.origin.x;
    float y = imageViewThatContainsVisualEffect.frame.origin.y - imageViewThatContainsImage.frame.origin.y;
    
    NSLog(@"Tamanho da imagem x: %f, y: %f", imagemA.size.width,imagemA.size.height );
    
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imagemA.size.width, imagemA.size.height), YES, 0.0);
    
    [imagemA drawAtPoint: CGPointMake(0,0)];
    
    [imagemB drawAtPoint: CGPointMake(x,y)
               blendMode: kCGBlendModeNormal // you can play with this
                   alpha: 1];
    
    UIImage *answer = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    currentImage = answer;
    
    [historicoImagens addObject:answer];
    
    return answer;
        
}



@end
