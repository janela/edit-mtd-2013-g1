//
//  NCCategoryViewController.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 02/07/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCCategoryViewController.h"

@interface NCCategoryViewController ()

@end

@implementation NCCategoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)categorySelected:(id)sender {
    
    NSMutableArray * arrayComTagsId = [[NSMutableArray alloc]init];
    
    for (int i = 1; i <=10; i++) {
        
        UIButton * button = (UIButton*)[self.view viewWithTag:i];
        
        if(button.selected){
            
            [arrayComTagsId addObject:[NSString stringWithFormat:@"%i",i]];
            
            
        }
        
    }
    
    
    [self.delegate categorySelected:arrayComTagsId];

    
    
    
}

- (void)buttonPressed:(id)sender{
    
    UIButton * button = (UIButton*)sender;
    
    if(!button.selected){
    
        [button setSelected:YES];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
        
    }
    else{
        
        [button setSelected:NO];
        [button setTitleColor:[UIColor blueColor] forState:UIControlStateSelected];
        
        
        
    }
    
}
@end
