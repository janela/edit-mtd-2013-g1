//
//  NCArtworkDetailViewController.m
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 12/06/2013.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCArtworkDetailViewController.h"
#import "NCAuthor.h"
#import "NCAuthorDetailViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCArtworkManager.h"
#import "NCFullScreenViewController.h"
#import <AddressBook/AddressBook.h>


@interface NCArtworkDetailViewController ()
{
    NCAuthor * currentAuthor;
    
}


@end

@implementation NCArtworkDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
   
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    [_eraseObra setHidden:YES];
    
    
    [[NCArtworkManager sharedManager] getAuthorWithID:_artwork.author.identity withCompletionBlock:^(NSError * error, NCAuthor * author){
        
        if([_artwork.author.identity isEqualToString:author.identity] )
            [_eraseObra setHidden:NO];
        
       
        
        
        NSDictionary * tagsDictionary = [self getTagsDictionary];
        NSString * tagsString= @"";
       
        for(NSString * string  in _artwork.tags){
            
            NSString * stringid2 = [NSString stringWithFormat:@"%@",string];
            
            NSString * stringid = [tagsDictionary objectForKey:stringid2];
            
            tagsString = [tagsString stringByAppendingFormat:@"%@ ",stringid];
         }
        

        
        
        
        _outletTitle.text = _artwork.title;
        
        _topBar.title = _artwork.title;
        
        _outletTags.text =  tagsString;
        _outletAuthor.text = author.name;
        _outletDescription.text = @"";
        [self setLocalOfArtwork:self];
        
        _outletImage.image = _artwork.image;
        
        currentAuthor = author;
        
        if(_hideButton){
            
            [self.buttonShowAuthorDetail setHidden:YES];
            
        }
      
    }];
       
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if([self.navigationController.viewControllers indexOfObject:self] == NSNotFound){
        [self.artworkDetailViewControllerDelegate artworkDetailViewControllerWillDismiss:self];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}



- (IBAction)actionSeeAuthorDetail:(id)sender {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ShowAuthorDetail"])
    {
        NCAuthorDetailViewController * advc = [segue destinationViewController];
        
        [advc setAuthor:currentAuthor];
        
        
    }
    else if( [[segue identifier] isEqualToString:@"ShowFullScreen"]){
        
        
        
        NCFullScreenViewController * fsvc = [segue destinationViewController];
        
        [fsvc setImageFullScreen:_artwork.image];

        
    }
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

- (IBAction)buttonDeleteArtwork:(id)sender {
    
    
    [[NCArtworkManager sharedManager] deleteArtwork:_artwork fromAuthor:_artwork.author];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    
}

-(void)  setLocalOfArtwork: (NCArtworkDetailViewController*) nadvc{

    CLGeocoder *geocoder = [[CLGeocoder alloc] init];

    CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:_artwork.coordinate.latitude
                                                    longitude:_artwork.coordinate.longitude];
    
    

    [geocoder reverseGeocodeLocation:newLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                   
                       if (error) {
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                   
                   if (placemarks && placemarks.count > 0)
                   {
                       CLPlacemark *placemark = placemarks[0];
                       
                       NSDictionary *addressDictionary =
                       placemark.addressDictionary;
                       
                       NSLog(@"%@ ", addressDictionary);
                       NSString *address = [addressDictionary
                                            objectForKey:(NSString *)kABPersonAddressStreetKey];
                       NSString * city = [addressDictionary
                                         objectForKey:(NSString *)kABPersonAddressCityKey];
                       
                       nadvc.outletDescription.text = [NSString stringWithFormat:@"%@,%@", city,address ];
                       
                       
                       NSString *state = [addressDictionary
                                          objectForKey:(NSString *)kABPersonAddressStateKey];
                       NSString *zip = [addressDictionary
                                        objectForKey:(NSString *)kABPersonAddressZIPKey];
                       
                       
                       NSLog(@"%@ %@ %@ %@", address,city, state, zip);
                   }
                   
               }];


    
    
}

-(NSDictionary*) getTagsDictionary{
    
    
    
    
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    
    
    
    [dic setObject:@"abstract" forKey:@"1"];
    
    [dic setObject:@"urban" forKey:@"2"];
    
    [dic setObject:@"nature" forKey:@"3"];
    
    [dic setObject:@"fashion" forKey:@"4"];
    
    [dic setObject:@"people" forKey:@"5"];
    
    [dic setObject:@"family" forKey:@"6"];
    
    [dic setObject:@"art" forKey:@"7"];
    
    [dic setObject:@"cartoon" forKey:@"8"];
    
    [dic setObject:@"photography" forKey:@"9"];
    
    [dic setObject:@"other" forKey:@"10"];
    
    
    
    return dic;
    
    
    
}


@end
