//
//  NCAuthorDetailViewController.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 17/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCAuthorDetailViewController.h"
#import "NCArtworkCell.h"
#import "NCArtwork.h"
#import "NCArtworkManager.h"
#import "NCArtworkDetailViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCImageCacheManager.h"
#import "EDURLConnectionLoader.h"

@interface NCAuthorDetailViewController ()
{
    
    NSArray * artworkCollection;
    
    
}
@end

@implementation NCAuthorDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
	// Do any additional setup after loading the view.
    
    NSString * userId = [self.author.identity description];
    
    [[NCArtworkManager sharedManager] getAllArtworksForUserId:userId withCompletionBlock:^(NSError * error, NSArray * array){
        
        artworkCollection = [[NSArray alloc]initWithArray:array];
        
        [self.collectionView reloadData];
        
        NSUInteger count = [artworkCollection count];
        
        [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:artworkCollection];
        
        
        self.labelNumberOfArtworks.text = [NSString stringWithFormat:@"Número de obras: %lu",(unsigned long)count ];
    }];

    NSString * urlPicture = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture",_author.facebookid];
    
    NSURL * url = [NSURL URLWithString:urlPicture];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    
    EDURLConnectionLoader * loader = [[EDURLConnectionLoader alloc]initWithRequest:request];
    
    loader.completionBlock = ^(NSError*error, NSData * data){
        
        self.imageProfilePicture.image = [UIImage imageWithData:data];

        
    };
    
    [loader load];
    
    self.labelAuthorName.text = self.author.name;
    
    
    //Pedir a foto do facebook
    
    
    
    
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Collection View Datasource Delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [artworkCollection count];
    
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NCArtworkDetailViewController *launchViewController = (NCArtworkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ShowArtworkDetailView"];
    
    [launchViewController setArtwork:[artworkCollection objectAtIndex:indexPath.row]];
    
    [launchViewController setHideButton:YES];
    [[self navigationController] pushViewController:launchViewController animated:YES];
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NCArtworkCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NCArtworkCell" forIndexPath:indexPath];
    
    cell.artwork = [artworkCollection objectAtIndex:indexPath.row];
    
    cell.imageView.image = cell.artwork.image;
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
    
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
     // 2
    CGSize retval = /**artwork..thumbnail.size.width > 0 ? photo.thumbnail.size : */CGSizeMake(100, 100);
    retval.height += 35; retval.width += 35; return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

- (void)allArtworkImagesDownloaded{
    
    [self.collectionView reloadData];
    
}

@end
