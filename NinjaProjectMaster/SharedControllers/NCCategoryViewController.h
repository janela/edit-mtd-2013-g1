//
//  NCCategoryViewController.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 02/07/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCCategoryPopoverDelegate.h"

@interface NCCategoryViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *outletAbstract;

@property (weak, nonatomic) IBOutlet UIButton *outletUrban;


@property (weak, nonatomic) IBOutlet UIButton *outletNature;


@property (weak, nonatomic) IBOutlet UIButton *Fashion;


@property (weak, nonatomic) IBOutlet UIButton *outletPeople;


@property (weak, nonatomic) IBOutlet UIButton *Family;

@property (weak, nonatomic) IBOutlet UIButton *outletArt;

@property (weak, nonatomic) IBOutlet UIButton *outletCartoon;

@property (weak, nonatomic) IBOutlet UIButton *outletPhotography;


@property (weak, nonatomic) IBOutlet UIButton *outletOther;


- (IBAction)categorySelected:(id)sender;

@property id<NCCategoryPopoverDelegate> delegate;


-(IBAction)buttonPressed:(id)sender;

@end
