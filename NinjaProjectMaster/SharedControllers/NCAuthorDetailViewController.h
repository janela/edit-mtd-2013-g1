//
//  NCAuthorDetailViewController.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 17/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCAuthor.h"
#import "NCImageCacheDelegate.h"

@interface NCAuthorDetailViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,NCImageCacheDelegate>

@property (strong,nonatomic) NCAuthor * author;

@property (weak, nonatomic) IBOutlet UIImageView *imageProfilePicture;

@property (weak, nonatomic) IBOutlet UILabel *labelAuthorName;


@property (weak, nonatomic) IBOutlet UILabel *labelNumberOfArtworks;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UINavigationItem *topBar;

- (IBAction)LogoutButtonPressed:(id)sender;

@end
