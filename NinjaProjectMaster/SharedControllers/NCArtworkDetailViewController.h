//
//  NCArtworkDetailViewController.h
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 12/06/2013.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NCArtwork.h"

@class NCArtworkDetailViewController;

@protocol NCArtworkDetailViewControllerDelegate <NSObject>

- (void)artworkDetailViewControllerWillDismiss:(NCArtworkDetailViewController*)artworkDetailViewController;

@end

@interface NCArtworkDetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UINavigationItem *topBar;

@property (weak, nonatomic) IBOutlet UILabel *outletTitle;

@property (weak, nonatomic) IBOutlet UILabel *outletDescription;

@property (weak, nonatomic) IBOutlet UIButton *buttonShowAuthorDetail;

@property (weak, nonatomic) IBOutlet UILabel *outletTags;

@property (weak, nonatomic) IBOutlet UIImageView *outletImage;

@property (weak, nonatomic) IBOutlet UILabel *outletAuthor;

@property (strong, nonatomic) IBOutlet UIButton *eraseObra;

@property (weak, nonatomic) id<NCArtworkDetailViewControllerDelegate> artworkDetailViewControllerDelegate;

@property BOOL hideButton;

@property (strong,nonatomic) NCArtwork * artwork;

- (IBAction)actionSeeAuthorDetail:(id)sender;

- (IBAction)LogoutButtonPressed:(id)sender;

- (IBAction)buttonDeleteArtwork:(id)sender;


@end
