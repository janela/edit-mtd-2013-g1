//
//  NCFullScreenViewController.m
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 19/06/2013.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCFullScreenViewController.h"

#define ZOOM_VIEW_TAG 100

#define ZOOM_STEP 1.5



@interface NCFullScreenViewController (UtilityMethods)



-(CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center;



@end


@implementation NCFullScreenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
	
    
    //Setting up the scrollView
    
    self.scrollView.bouncesZoom = YES;
    
    self.scrollView.clipsToBounds = YES;
    
    //Setting up the imageView
    
    _imageView.userInteractionEnabled = YES;
    _imageView.image = _imageFullScreen;
    
    _imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin);
    
    NSLog(@"size : %f . %f", _imageView.frame.size.width, _imageView.frame.size.height);
    //Adding the imageView to the scrollView as subView
    
    [self.scrollView addSubview:_imageView];
    
    self.scrollView.contentSize = CGSizeMake(_imageView.bounds.size.width, _imageView.bounds.size.height);
    
    
    
    self.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    
    
    
    //UITapGestureRecognizer set up
    
    //UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    
    UITapGestureRecognizer *twoFingerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTwoFingerTap:)];
    
    
    
    [doubleTap setNumberOfTapsRequired:2];
    
    [twoFingerTap setNumberOfTouchesRequired:2];
    
    
    
    //Adding gesture recognizer
    
    [_imageView addGestureRecognizer:doubleTap];
    
    [_imageView addGestureRecognizer:twoFingerTap];
    
    // calculate minimum scale to perfectly fit image width, and begin at that scale
    
    float minimumScale = 1.0;//This is the minimum scale, set it to whatever you want. 1.0 = default
    
    _scrollView.maximumZoomScale = 4.0;
    
    _scrollView.minimumZoomScale = minimumScale;
    
    _scrollView.zoomScale = minimumScale;
    
    [_scrollView setContentMode:UIViewContentModeCenter];
    
    [_scrollView setContentSize:CGSizeMake(_imageView.frame.size.width, _imageView.frame.size.height)];
    
    
    
}
- (void)scrollViewDidZoom:(UIScrollView *)aScrollView {
    
    CGFloat offsetX = (_scrollView.bounds.size.width > _scrollView.contentSize.width)?
    
    (_scrollView.bounds.size.width - _scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (_scrollView.bounds.size.height > _scrollView.contentSize.height)?
    
    (_scrollView.bounds.size.height - _scrollView.contentSize.height) * 0.5 : 0.0;
    
    _imageView.center = CGPointMake(_scrollView.contentSize.width * 0.5 + offsetX,
                                   
                                   _scrollView.contentSize.height * 0.5 + offsetY);
    
}



- (void)viewDidUnload {
    
    self.scrollView = nil;
    
    self.imageView = nil;
    
}

#pragma mark UIScrollViewDelegate methods



- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return _imageView;
    
}



#pragma mark TapDetectingImageViewDelegate methods



- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    // zoom in
    
    float newScale = [_scrollView zoomScale] * ZOOM_STEP;
    
    
    
    if (newScale > _scrollView.maximumZoomScale){
        
        newScale = _scrollView.minimumZoomScale;
        
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        
        
        [_scrollView zoomToRect:zoomRect animated:YES];
        
        
        
    }
    
    else{
        
        
        
        newScale = _scrollView.maximumZoomScale;
        
        CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
        
        
        
        [_scrollView zoomToRect:zoomRect animated:YES];
        
    }
    
}





- (void)handleTwoFingerTap:(UIGestureRecognizer *)gestureRecognizer {
    
    // two-finger tap zooms out
    
    float newScale = [_scrollView zoomScale] / ZOOM_STEP;
    
    CGRect zoomRect = [self zoomRectForScale:newScale withCenter:[gestureRecognizer locationInView:gestureRecognizer.view]];
    
    [_scrollView zoomToRect:zoomRect animated:YES];
    
}



#pragma mark Utility methods



- (CGRect)zoomRectForScale:(float)scale withCenter:(CGPoint)center {
    
    
    
    CGRect zoomRect;
    
    
    
    // the zoom rect is in the content view's coordinates.
    
    //    At a zoom scale of 1.0, it would be the size of the imageScrollView's bounds.
    
    //    As the zoom scale decreases, so more content is visible, the size of the rect grows.
    
    zoomRect.size.height = [_scrollView frame].size.height / scale;
    
    zoomRect.size.width  = [_scrollView frame].size.width  / scale;
    
    
    
    // choose an origin so as to get the right center.
    
    zoomRect.origin.x    = center.x - (zoomRect.size.width  / 2.0);
    
    zoomRect.origin.y    = center.y - (zoomRect.size.height / 2.0);
    
    
    
    return zoomRect;
    
    
    
}

- (IBAction)exitFullScreen:(id)sender {
    
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
