//
//  NCCategoryPopoverDelegate.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 02/07/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NCCategoryPopoverDelegate <NSObject>

@required
-(void) categorySelected:(NSArray*)array;
@end
