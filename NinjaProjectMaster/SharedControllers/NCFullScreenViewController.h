//
//  NCFullScreenViewController.h
//  NinjaProjectMaster
//
//  Created by EDIT Guest on 19/06/2013.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NCFullScreenViewController : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak,nonatomic) UIImage * imageFullScreen;

- (IBAction)exitFullScreen:(id)sender;

@end
