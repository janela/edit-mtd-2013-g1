//
//  NinjaMasterViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NinjaMasterViewController : UITableViewController <UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *imagem1;


@property (weak, nonatomic) IBOutlet UIImageView *imagem2;
@property (weak, nonatomic) IBOutlet UIImageView *imagem3;

@end




