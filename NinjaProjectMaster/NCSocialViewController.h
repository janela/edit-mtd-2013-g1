//
//  NinjaSocialViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaBaseDetailViewController.h"
#import "NinjaCriarMenuPopoverViewController.h"
#import "NCImageCacheDelegate.h"
#import "NCImageCacheManager.h"
#import "NCCategoryPopoverDelegate.h"

@interface NCSocialViewController : NinjaBaseDetailViewController <UIPopoverControllerDelegate, NinjaCriarMenuPopOverDelegate,  UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,NCImageCacheDelegate,NCCategoryPopoverDelegate,UIScrollViewDelegate>


@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NinjaCriarMenuPopoverViewController *criarMenuPopover;
@property (nonatomic, strong) UIPopoverController *criarImagemPopoverController;
@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSMutableArray *searchResults;
@property(nonatomic, strong) NSMutableArray *searches;

@property(nonatomic, weak) IBOutlet UITextField *textField;

- (IBAction)ShowCriarMenu:(id)sender;


- (IBAction)LogoutButtonPressed:(id)sender;

- (IBAction)selectTags:(id)sender;

@end
