//
//  NCLoginManager.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 26/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCLoginManager.h"
#import <Parse/Parse.h>
#import "NCArtworkManager.h"
#import "NCFileHandler.h"

@interface NCLoginManager()



@end


@implementation NCLoginManager

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void) doLogin{
    
    if([self isTheUserLoggedInFacebook]){
        
        //grabUserDictionary
        [self getFacebookUserDictionary];
        
        
        
    }
    else{
        
        [self performFacebookLogin];
        
        
    }

    
}


-(BOOL) isTheUserLoggedInFacebook{
    
    
    return ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]);

    
}

- (void) getFacebookUserDictionary{
    
    
    FBRequest *request = [FBRequest requestForMe];
    
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            _facebookUserInfo = (NSDictionary *)result;
            /**
             NSString *facebookID = userData[@"id"];
             NSString *name = userData[@"name"];
             NSString *location = userData[@"location"][@"name"];
             NSString *gender = userData[@"gender"];
             NSString *birthday = userData[@"birthday"];
             NSString *relationship = userData[@"relationship_status"];
             
             NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
             
             
            
            // Now add the data to the UI elements
            // ...
            */
        
        [self userInfoCatched];
            
            
            
                        
            
        }
        else{
            
            NSLog(@"Erro a obter dados de user: %@", error.description);
            
        }
    }];

    
    
    
    
    
}

- (void) performFacebookLogin {
    
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
    
    // Login PFUser using facebook
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        
        if (!user) {
            if (!error) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:@"Uh oh. The user cancelled the Facebook login." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            } else {
                NSLog(@"Uh oh. An error occurred: %@", error);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Log In Error" message:[error description] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Dismiss", nil];
                [alert show];
            }
        } else if (user.isNew) {
            NSLog(@"User with facebook signed up and logged in!");
            
        } else {
            
            [self getFacebookUserDictionary];
            NSLog(@"User with facebook logged in!");
                    }
    }];
    
}


-(void) userInfoCatched{
    
    [[PFUser currentUser] setObject:[_facebookUserInfo objectForKey:@"id"] forKey:@"facebookId"];
    
    NSString * facebookuserid = [_facebookUserInfo objectForKey:@"id"];
    
    
    [[NCArtworkManager sharedManager] validateAndCreateUser:facebookuserid withCompletionBlock:^(NSError*error,NSDictionary* userDictionary){
        
        if(!error){
            
            NSDictionary * dictionary = [userDictionary objectForKey:@"Author"];
            
            NCAuthor * currentAuthor = [[NCAuthor alloc]initFromDictionary:dictionary];
            
            //Persistência do user. Não funciona.
            //[NCFileHandler writeAuthorToDocuments:currentAuthor];
            
            [[NCArtworkManager sharedManager] setCurrentAuthor:currentAuthor];
            
            [_delegate loginFinished];
        }
        else{
            
            NSLog(@"Erro a validar utilizador!!!!");
        }
            
        
    }];
    
}



@end
