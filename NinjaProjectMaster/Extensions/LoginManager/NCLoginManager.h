//
//  NCLoginManager.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 26/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCLoginDelegate.h"
#import "NCAuthor.h"


@interface NCLoginManager : NSObject

    
@property (strong,nonatomic)   id <NCLoginDelegate> delegate;

@property (strong,nonatomic) NSDictionary * facebookUserInfo;

@property (strong,nonatomic) NCLoginManager * sharedManager;

-(void) doLogin;

-(BOOL) isTheUserLoggedInFacebook;

- (void) getFacebookUserDictionary;

@end
