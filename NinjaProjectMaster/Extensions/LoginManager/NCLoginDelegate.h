//
//  NCLoginDelegate.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 26/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NCLoginDelegate <NSObject>

@required

- (void) loginFinished;

@end
