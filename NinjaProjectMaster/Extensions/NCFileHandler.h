//
//  NCFileHandler.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 16/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCAuthor.h"

@interface NCFileHandler : NSObject

+(void) writeImageToDocuments: (UIImage*)image withName:(NSString*) name;

+(void) writeAuthorToDocuments: (NCAuthor*) author;

+(NCAuthor*) loadAuthorFromDocuments;

+(BOOL) fileExists:(NSString*) filename;

+(void) writeArrayToDocuments:(NSArray*) artwork toFile:(NSString*) fileName;

+(NSArray*) loadArrayFromDocuments:(NSString*) filename;

@end
