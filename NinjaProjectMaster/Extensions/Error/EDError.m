//
//  EDError.m
//  EDITStub
//
//  Created by Tiago Janela on 27/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//
#import "EDError.h"

NSError* EDCreateErrorWithCodeAndMessage(edArtworkManager_Error errorCode, NSString *errorMessage)
{
	NSDictionary *dictionary = [NSDictionary dictionaryWithObject:errorMessage
																												 forKey:kEDArtworkManager_Error_MessageUserInfoKey];
	
	return [NSError errorWithDomain:kEDArtworkManager_ErrorDomain
														 code:errorCode userInfo:dictionary];
}