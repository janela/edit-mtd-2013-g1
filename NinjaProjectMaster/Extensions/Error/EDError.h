//
//  EDError.h
//  EDITStub
//
//  Created by Tiago Janela on 27/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#ifndef __EDError__
#define __EDError__


#define kEDArtworkManager_ErrorDomain @"EDArtworkManager"

#define kEDArtworkManager_Error_MessageUserInfoKey @"EDError_Message"

typedef enum {
	kEDArtworkManager_Error_System = 0x100,
	kEDArtworkManager_Error_NoNetwork = 0x101,
	kEDArtworkManager_Error_ServerError = 0x102,
	kEDArtworkManager_Error_UserCancelled = 0x103
}edArtworkManager_Error;

NSError* EDCreateErrorWithCodeAndMessage(edArtworkManager_Error errorCode, NSString *errorMessage);

#endif
