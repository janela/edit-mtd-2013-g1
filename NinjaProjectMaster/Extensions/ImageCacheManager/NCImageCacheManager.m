//
//  NCImageCacheManager.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 29/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCImageCacheManager.h"
#import "EDURLConnectionLoader.h"
#import "NCArtwork.h"

@implementation NCImageCacheManager
{
    
    NSMutableDictionary * urlAndUImages;
    
    
}

static NCImageCacheManager* _sharedManager;


- (id)init
{
	self = [super init];
	if (self) {
        
        urlAndUImages = [[NSMutableDictionary alloc]init];
        
		
	}
	return self;
}

+ (NCImageCacheManager *)sharedManager{
	if(!_sharedManager){
		_sharedManager = [[NCImageCacheManager alloc] init];
	}
	return _sharedManager;
}

-(void) ncImageCacheDelegate:(id<NCImageCacheDelegate>)delegate arrayWithArtworksForImageDownloading:(NSArray*)array{
    
    
    for (NCArtwork * artwork in array) {
        if(artwork.image == nil){
            
            artwork.image = [self getImageFromURL:artwork.imageUrl];
            
        }
       
    }
    [delegate allArtworkImagesDownloaded];
    
    

    
}


-(UIImage*) getImageFromURL:(NSString*) url{
    
    __block UIImage * image = [urlAndUImages objectForKey:url];
    
    if(image==nil){
        
        NSURLRequest * request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        
        EDURLConnectionLoader * loader = [[EDURLConnectionLoader alloc]initWithRequest:request];
        
        
        loader.completionBlock = ^(NSError* error,NSData* responseData){
            
            image = [UIImage imageWithData:responseData];
            [urlAndUImages setObject:image forKey:url];
            
        };
        
        [loader load];
        
    }
    while(image==nil){
        
        NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0.1];
        [[NSRunLoop currentRunLoop] runUntilDate:date];

    }
    
    return image;
    
    
    
    
}



@end
