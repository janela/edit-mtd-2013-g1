//
//  NCImageCacheManager.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 29/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCImageCacheDelegate.h"

@interface NCImageCacheManager : NSObject

+ (NCImageCacheManager*)sharedManager;

-(void) ncImageCacheDelegate:(id<NCImageCacheDelegate>)delegate arrayWithArtworksForImageDownloading:(NSArray*)array;

-(UIImage*) getImageFromURL:(NSString*) url;

@end
