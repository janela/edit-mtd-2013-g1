//
//  NCImageCacheDelegate.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 29/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NCImageCacheDelegate <NSObject>

@required
-(void) allArtworkImagesDownloaded;


@end
