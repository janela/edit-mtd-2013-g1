//
//  NCFileHandler.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 16/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCFileHandler.h"
#import "NCAuthor.h"
#import "NCArtwork.h"

@implementation NCFileHandler

+(void) writeImageToDocuments: (UIImage*)image withName:(NSString*) filename{
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];
    //create content - four lines of text
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:filePath atomically:YES];
    
    
    
    
}

+(void) writeAuthorToDocuments: (NCAuthor*) author{
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/author.dat",
                          documentsDirectory];
    
    [NSKeyedArchiver archiveRootObject:author toFile:filePath];
    
    
}

+(void) writeArrayToDocuments:(NSArray*) artwork toFile:(NSString*) fileName{
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,fileName];

    
    NSData * arrayData = [NSKeyedArchiver archivedDataWithRootObject:artwork];
    
    
    [arrayData writeToFile:filePath atomically:YES];
    
    
    
    
    
}

+(NSArray*) loadArrayFromDocuments:(NSString*) filename{
    
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];
    
    NSData * arrayData = [NSData dataWithContentsOfFile:filePath];
    
    NSArray * array = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
    
    return array;
    
    
    
}


+(NCAuthor*) loadAuthorFromDocuments{
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/author.dat",
                          documentsDirectory];
    
    NCAuthor * author;
    
    if ([self fileExists:@"author.dat"]){
        author = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    }
    return author;
    
}

+(BOOL) fileExists:(NSString*) filename{
    
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];

    
    //make a file name to write the data to using the documents directory:
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",
                          documentsDirectory,filename];

    
    NSFileManager *filemgr;
    
    filemgr = [NSFileManager defaultManager];
    
    return [filemgr fileExistsAtPath: filePath ];
    
        
}
@end
