//
//  EDBaseResponse.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EDBaseResponse : NSObject

@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) id data;

+ (EDBaseResponse*) parseResponseFromDictionary:(NSDictionary*)dictionary;

@end
