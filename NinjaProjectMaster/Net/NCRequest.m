//
//  NCRequest.m
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 27/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCRequest.h"
#import "NSThread+MCSMAdditions.h"

@interface NCRequest()
{
    
    NSMutableData * responseData;
    
    NSURLRequest * request;
    
    NSURLConnection *_underlyingConnection;
	
    BOOL _exit;
    
    
    
}


@end


@implementation NCRequest

- (id)initWithRequest:(NSURLRequest*) incomingRequest
{
    self = [super init];
    if (self) {
        // Create the request.
        request = incomingRequest;
        // Create url connection and fire request
        
    }
    return self;
}


#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

-(void) startConnection{
    
    [NSThread MCSM_performBlockInBackground:^{
		NSURLConnection *conn = [[NSURLConnection alloc] init];
        (void)[conn initWithRequest:request delegate:self startImmediately:YES];
        
        while(!_exit){
            @autoreleasepool {
                NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0.1];
                [[NSRunLoop currentRunLoop] runUntilDate:date];
            }
        }
	}];
    
    
}

@end
