//
//  NCRequest.h
//  NinjaProjectMaster
//
//  Created by Rui Bordadágua on 27/06/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

typedef void(^EDURLConnectionLoaderCompletionHandler)(NSError*,NSData*);


#import <Foundation/Foundation.h>

@interface NCRequest : NSObject <NSURLConnectionDelegate>



@end
