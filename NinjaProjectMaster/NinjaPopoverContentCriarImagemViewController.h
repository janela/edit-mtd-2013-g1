//
//  NinjaPopoverContentCriarImagemViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/29/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NinjaPopoverContentCriarImagemViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@end
