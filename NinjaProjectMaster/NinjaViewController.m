//
//  NinjaViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaViewController.h"

@interface NinjaViewController ()

@end

@implementation NinjaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
