//
//  NinjaAppDelegate.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaAppDelegate.h"
#import "NinjaLoginViewController.h"
#import "NinjaDetailViewController.h"
#import "NCArtworkManager.h"

#import <Parse/Parse.h>

@implementation NinjaAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    // Parse Facebook Login
    [Parse setApplicationId:@"huxUbK4n0osXK977dO4ZbmHJar0IgZSuRpf7sM5k"
                  clientKey:@"mpmqkrH4kOkjOzBMJrJ7yAvsU3fnU05KRdhmOE5S"];
    
    [PFFacebookUtils initializeFacebook];
    
    // Override point for customization after application launch.
    UISplitViewController *splitViewController = (UISplitViewController *)self.window.rootViewController;
    UINavigationController *navigationController = [splitViewController.viewControllers lastObject];
    splitViewController.delegate = (id)navigationController.topViewController;
    
        
    
    return YES;
    
}

							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[NCArtworkManager sharedManager] saveArraysToDocuments];
    
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

// ****************************************************************************
// App switching methods to support Facebook Single Sign-On.
// ****************************************************************************
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [PFFacebookUtils handleOpenURL:url];
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    static dispatch_once_t onceToken;
    
    dispatch_once( &onceToken, ^
                  {
                      UIStoryboard *storyboard = self.window.rootViewController.storyboard;
                      NinjaLoginViewController* launchViewController = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                      [self.window.rootViewController presentViewController:launchViewController animated:NO completion:NULL];
                      
                      
                  } );
    
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSession.activeSession handleDidBecomeActive];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    
    [FBSession.activeSession close];
    
}



@end
