//
//  NinjaLoginViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NinjaBaseDetailViewController.h"
#import "NCLoginDelegate.h"

@interface NinjaLoginViewController : NinjaBaseDetailViewController <NCLoginDelegate>

- (IBAction)dismissLogin:(id)sender;

@end
