//
//  NinjaDetailViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaDetailViewController.h"
#import "UISplitViewController+DetailViewSwapper.h"
#import "NinjaAppDelegate.h"
#import "NinjaCriarImagemViewController.h"
#import "NinjaCriarMenuPopoverViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCLoginManager.h"
#import "NCImageCacheManager.h"

@interface NinjaDetailViewController ()

@end

@implementation NinjaDetailViewController

@synthesize carouselMinhas;
@synthesize carouselUltimas;
@synthesize items;
@synthesize itemsUltimas;


- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    
    /**
         */
    
//    
//    NCArtworkManager * artworkManagerUltimas = [NCArtworkManager sharedManager];
//    NSArray * arrayUltimas = [[NSArray alloc]initWithObjects:@"surf", nil];
//    [artworkManagerUltimas getAllArtworksForTags:arrayUltimas withCompletionBlock:^(NSError*error, NSArray * array){
//        
//        self.itemsUltimas = [[NSMutableArray alloc]initWithArray:array];
//        
//        [carouselUltimas reloadData];
//    }];
    
}


- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    //this is true even if your project is using ARC, unless
    //you are targeting iOS 5 as a minimum deployment target
    carouselMinhas.delegate = nil;
    carouselMinhas.dataSource = nil;
    carouselUltimas.delegate = nil;
    carouselUltimas.dataSource = nil;
    
}



- (void)selectedImageSource:(UITableViewCell *)newImageSource{
    // Método para fazer escolher para chamar câmara ou biblioteca de imagens
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NinjaDetailViewController *startViewControler;
    NinjaCriarImagemViewController *destinationViewControler;
    
    startViewControler = (NinjaDetailViewController *)segue.sourceViewController;
    destinationViewControler = (NinjaCriarImagemViewController *)segue.destinationViewController;
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated{
    
    NCAuthor * currentAuthor = [[NCArtworkManager sharedManager] currentAuthor];
    
    
    
    [[NCArtworkManager sharedManager] getAllArtworksForUserId:currentAuthor.identity withCompletionBlock:^(NSError * error, NSArray * array){
        
        
        if([array count]>0){
        
        self.items = [[NSArray alloc]initWithArray:array];
        
        NSRange range;
        
        range.location = 0;
        
        if([array count]<5){
            
            range.length = [array count];
            
        }
        else
            range.length = 5;
        
        NSIndexSet * setIndexed = [[NSIndexSet alloc] initWithIndexesInRange:range];
        
        self.items = [self.items objectsAtIndexes:setIndexed];
        
        [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:self.items];
        }
        
        else
            self.items = [[NSArray alloc]init];
        
    }];
    
    [[NCArtworkManager sharedManager] getAllArtworksOrderedByDateWithCompletionBlock:^(NSError * error, NSArray * results){
        
        if(results && [results count] > 0){
            
            self.itemsUltimas = [[NSMutableArray alloc]initWithArray:results];
            
            NSRange range;
            
            range.location = 0;
            range.length = 5;
            
            NSIndexSet * setIndexed = [[NSIndexSet alloc] initWithIndexesInRange:range];
            
            self.itemsUltimas = [self.itemsUltimas objectsAtIndexes:setIndexed];
            
            
            
            [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:self.itemsUltimas];
            
        }
        else{
            
            NSLog(@"Error searching %@", error.description);
        }
        
    }];


    
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    //configure carousel
    carouselMinhas.type = iCarouselTypeRotary;
    carouselUltimas.type = iCarouselTypeRotary;
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    

    
    
    
    
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    //free up memory by releasing subviews
    self.carouselMinhas = nil;
    self.carouselUltimas = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}


- (IBAction)ShowCriarMenu:(id)sender {
    if (_criarMenuPopover == nil) {
        // Cria o NinjaCriarMenuPopoverViewController
        _criarMenuPopover = [[NinjaCriarMenuPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
        
        // Set o VC como delegate
        _criarMenuPopover.delegate = self;
    }
    
    if (_criarImagemPopoverController == nil) {
        // Se o Popover não estiver visivel, mostra-o
        _criarImagemPopoverController = [[UIPopoverController alloc] initWithContentViewController:_criarMenuPopover];
        
        [_criarImagemPopoverController presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        // Se o popover estiver visivel, esconde-o
        [_criarImagemPopoverController dismissPopoverAnimated:YES];
        _criarImagemPopoverController = nil;
    }
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    if (carousel == carouselMinhas)
    {
        return [items count];
    }
    else
    {
        return [itemsUltimas count];
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    //UILabel *label = nil;
    UIImage *imagem = nil;
    UIButton *button = (UIButton *)view;
	if (button == nil)
        
    {
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 150.0f)];
        
        view.contentMode = UIViewContentModeScaleAspectFit;
        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0.0f, 0.0f, 200.0f, 150.0f);
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        if (carousel == carouselMinhas) {
            NCArtwork*artwork= [items objectAtIndex:index];
            imagem = artwork.image;
            
            ((UIImageView *)view).image = imagem;
            
            [button setBackgroundImage:imagem forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        }else{
            NCArtwork*artworkUltimas = [itemsUltimas objectAtIndex:index];
            imagem = artworkUltimas.image;
            
            ((UIImageView *)view).image = imagem;
            [button setBackgroundImage:imagem forState:UIControlStateNormal];
            [button addTarget:self action:@selector(buttonUltimasTapped:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    }
    else
    {
    }
    return button;
}


#pragma mark -
#pragma mark Button tap event

- (void)buttonTapped:(UIButton *)sender
{
    NSLog(@"Botão Minhas");
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NCArtworkDetailViewController *launchViewController = (NCArtworkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ShowArtworkDetailView"];
    launchViewController.artworkDetailViewControllerDelegate = self;
    NSInteger index = [carouselMinhas indexOfItemViewOrSubview:sender];
    NCArtwork *artwork = [self.items objectAtIndex:index];
    [launchViewController setArtwork:artwork];
    
    
    UINavigationController * nvc = self.navigationController;
    [nvc pushViewController:launchViewController animated:YES];
    
    
}

- (void)artworkDetailViewControllerWillDismiss:(NCArtworkDetailViewController *)artworkDetailViewController{
    [carouselMinhas reloadData];
    [carouselUltimas reloadData];
}

- (void)buttonUltimasTapped:(UIButton *)sender
{
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NCArtworkDetailViewController *launchViewController = (NCArtworkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ShowArtworkDetailView"];
    launchViewController.artworkDetailViewControllerDelegate = self;
    NSInteger index = [carouselUltimas indexOfItemViewOrSubview:sender];
    NCArtwork *artwork = [self.itemsUltimas objectAtIndex:index];
    [launchViewController setArtwork:artwork];
    
    
    UINavigationController * nvc = self.navigationController;
    [nvc pushViewController:launchViewController animated:YES];
    
    
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionFadeMin:
            return -0.3;
        case iCarouselOptionFadeMax:
            return 0.3;
        case iCarouselOptionFadeRange:
            return 2.0;
        default:
            return value;
    }
}

- (void)allArtworkImagesDownloaded{
    
    [carouselMinhas reloadData];
    [carouselUltimas reloadData];
    
    
}
@end
