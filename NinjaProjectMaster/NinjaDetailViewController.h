//
//  NinjaDetailViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "NinjaBaseDetailViewController.h"
#import "NinjaCriarMenuPopoverViewController.h"
#import "iCarousel.h"
#import "NCArtworkManager.h"

@interface NinjaDetailViewController : NinjaBaseDetailViewController <UIPopoverControllerDelegate, NinjaCriarMenuPopOverDelegate>

@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, retain) NSMutableArray *itemsUltimas;

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) NinjaCriarMenuPopoverViewController *criarMenuPopover;
@property (nonatomic, strong) UIPopoverController *criarImagemPopoverController;

@property (nonatomic, retain) IBOutlet iCarousel *carouselMinhas;


@property (nonatomic, retain) IBOutlet iCarousel *carouselUltimas;


- (IBAction)ShowCriarMenu:(id)sender;

- (IBAction)LogoutButtonPressed:(id)sender;

@end