//
//  NinjaCriarMenuPopoverViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 6/3/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NinjaCriarMenuPopOverDelegate <NSObject>
@required
-(void)selectedImageSource:(UITableViewCell *)newImageSource;
@end

@interface NinjaCriarMenuPopoverViewController : UITableViewController

@property (nonatomic,strong) NSMutableArray *sourceImageNames;
@property (nonatomic, strong) id<NinjaCriarMenuPopOverDelegate> delegate;
@end
