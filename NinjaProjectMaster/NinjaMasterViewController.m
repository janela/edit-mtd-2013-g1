//
//  NinjaMasterViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaMasterViewController.h"
#import "UISplitViewController+DetailViewSwapper.h"

@interface NinjaMasterViewController ()

@end

@implementation NinjaMasterViewController
@synthesize tableView;

@synthesize imagem1, imagem2, imagem3;


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    [self.splitViewController prepareReplaceSegueFor:segue.destinationViewController];
    
}

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        
        UIImageView * bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portraitFundoMenu.jpg"]];
        
        [tableView setBackgroundView:bgImage];
        
    }else{
        
        // code for landscape orientation
        UIImageView * bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"landscapeFundoMenu.jpg"]];
        
        [tableView setBackgroundView:bgImage];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
   
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        UIImageView * bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portraitFundoMenu.jpg"]];
        
        [tableView setBackgroundView:bgImage];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        UIImageView * bgImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"landscapeFundoMenu.jpg"]];
        
        [tableView setBackgroundView:bgImage];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        self.imagem1.image = [UIImage imageNamed:@"btn_homeSelected.png"];
        self.imagem2.image = [UIImage imageNamed:@"btn_minhasImagens.png"];
        self.imagem3.image = [UIImage imageNamed:@"btn_social.png"];
    }
    else if (indexPath.row == 1) {
        self.imagem1.image = [UIImage imageNamed:@"btn_home.png"];
        self.imagem2.image = [UIImage imageNamed:@"btn_minhasImagensSelected.png"];
        self.imagem3.image = [UIImage imageNamed:@"btn_social.png"];
    }
    else if (indexPath.row == 2) {
        self.imagem1.image = [UIImage imageNamed:@"btn_home.png"];
        self.imagem2.image = [UIImage imageNamed:@"btn_minhasImagens.png"];
        self.imagem3.image = [UIImage imageNamed:@"btn_socialSelected.png"];
    }
}



@end
