//
//  NinjaDetailViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaDetailViewController.h"
#import "UISplitViewController+DetailViewSwapper.h"
#import "NinjaAppDelegate.h"
#import "NinjaCriarImagemViewController.h"
#import "NinjaCriarMenuPopoverViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"

@interface NinjaDetailViewController () 

@end

@implementation NinjaDetailViewController

@synthesize carousel;
@synthesize items;


- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    
    self.items = [NSMutableArray array];
    int i;
    for(i=0; i<5; i++){
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"ninja%d",i] ofType:@"jpg"];
        UIImage* image =  (UIImage*)[UIImage imageWithContentsOfFile:filePath];
        
        [items addObject:image];
    }
}


- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    //this is true even if your project is using ARC, unless
    //you are targeting iOS 5 as a minimum deployment target
    carousel.delegate = nil;
    carousel.dataSource = nil;
    
}



- (void)selectedImageSource:(UITableViewCell *)newImageSource{
    // Método para fazer escolher para chamar câmara ou biblioteca de imagens
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NinjaDetailViewController *startViewControler;
    NinjaCriarImagemViewController *destinationViewControler;
    
    startViewControler = (NinjaDetailViewController *)segue.sourceViewController;
    destinationViewControler = (NinjaCriarImagemViewController *)segue.destinationViewController;
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //configure carousel
    carousel.type = iCarouselTypeCoverFlow2;
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"ninjahome.jpg"] drawInRect:self.view.bounds];
    UIImage *bgimage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:bgimage];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (IBAction)ShowCriarMenu:(id)sender {
    if (_criarMenuPopover == nil) {
        // Cria o NinjaCriarMenuPopoverViewController
        _criarMenuPopover = [[NinjaCriarMenuPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
        
        // Set o VC como delegate
        _criarMenuPopover.delegate = self;
    }
    
    if (_criarImagemPopoverController == nil) {
        // Se o Popover não estiver visivel, mostra-o
        _criarImagemPopoverController = [[UIPopoverController alloc] initWithContentViewController:_criarMenuPopover];
        
        [_criarImagemPopoverController presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        // Se o popover estiver visivel, esconde-o
        [_criarImagemPopoverController dismissPopoverAnimated:YES];
        _criarImagemPopoverController = nil;
    }
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}


#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [items count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    //UILabel *label = nil;
    UIImage *imagem = nil;
    UIButton *button = (UIButton *)view;
	if (button == nil)
        
    {
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 300.0f, 200.0f)];
        imagem = [items objectAtIndex:index];
        ((UIImageView *)view).image = imagem;
        view.contentMode = UIViewContentModeScaleAspectFill;
        button = [UIButton buttonWithType:UIButtonTypeCustom];
		button.frame = CGRectMake(0.0f, 0.0f, 300.0f, 200.0f);
		[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[button setBackgroundImage:imagem forState:UIControlStateNormal];
        [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
    }
    
    return button;
}


#pragma mark -
#pragma mark Button tap event

- (void)buttonTapped:(UIButton *)sender
{
    
    
	//get item index for button
	NSInteger index = [carousel indexOfItemViewOrSubview:sender];
	
    [[[UIAlertView alloc] initWithTitle:@"Seleccionada Imagem"
                                message:[NSString stringWithFormat:@"Foi seleccionada a imagem do Array numero %i", index]
                               delegate:nil
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:nil] show];
     
    
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionFadeMin:
            return -0.3;
        case iCarouselOptionFadeMax:
            return 0.3;
        case iCarouselOptionFadeRange:
            return 2.0;
        default:
            return value;
    }
}



@end
