//
//  NinjaSocialViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCSocialViewController.h"
#import "UISplitViewController+DetailViewSwapper.h"
#import "NinjaAppDelegate.h"
#import "NinjaCriarImagemViewController.h"
#import "NinjaCriarMenuPopoverViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCArtworkManager.h"
#import "NCArtworkCell.h"
#import "NCArtwork.h"
#import "NCArtworkDetailViewController.h"
#import "NCMapViewController.h"
#import "NCCategoryViewController.h"
#import "UIImage+animatedGIF.h"

@interface NCSocialViewController ()
{
    
    NCArtwork * currentArtwork;
    
    UIPopoverController * categoryPopover;
    
    int firstWork;
    
    UIImageView * loadingImage;
    
}


@end

@implementation NCSocialViewController

- (void)selectedImageSource:(UITableViewCell *)newImageSource{
    // Método para fazer escolher para chamar câmara ou biblioteca de imagens
    
    
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    UIImage *textFieldImage = [[UIImage imageNamed:@"search_field.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [self.textField setBackground:textFieldImage];
    
    firstWork = 0;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"loadingGIF" withExtension:@"gif"];
    
    loadingImage = [[UIImageView alloc]initWithFrame:CGRectMake(250,250,  200, 200)];
    
    loadingImage.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    
    [self.view addSubview:loadingImage];
    
    [[NCArtworkManager sharedManager] getWorksOrderedByDateFrom:firstWork to:firstWork+5 withCompletionBlock:^(NSError*error, NSArray*results){
     
     
     if(results && [results count] > 0){
        
        NSLog(@"Found %d photos matching.", [results count]);
        
        self.searchResults = [[NSMutableArray alloc]initWithArray:results];
        
        [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:self.searchResults];
        
    }
     else{
         
         NSLog(@"Error searching %@", error.description);
     }
     
     }];
    
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ShowCriarMenu:(id)sender {
    if (_criarMenuPopover == nil) {
        // Cria o NinjaCriarMenuPopoverViewController
        _criarMenuPopover = [[NinjaCriarMenuPopoverViewController alloc] initWithStyle:UITableViewStylePlain];
        
        // Set o VC como delegate
        _criarMenuPopover.delegate = self;
    }
    
    if (_criarImagemPopoverController == nil) {
        // Se o Popover não estiver visivel, mostra-o
        _criarImagemPopoverController = [[UIPopoverController alloc] initWithContentViewController:_criarMenuPopover];
        
        [_criarImagemPopoverController presentPopoverFromRect:[sender bounds] inView:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        
    } else {
        // Se o popover estiver visivel, esconde-o
        [_criarImagemPopoverController dismissPopoverAnimated:YES];
        _criarImagemPopoverController = nil;
    }

}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [self.searchResults count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NCArtworkCell * cell = [cv dequeueReusableCellWithReuseIdentifier:@"ArtworkCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    NCArtwork * artwork = [self.searchResults objectAtIndex:indexPath.row];
    cell.artwork = artwork;
    cell.imageView.image = cell.artwork.image;
    return cell;
}
// 4
/*- (UICollectionReusableView *)collectionView:
 (UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
 {
 return [[UICollectionReusableView alloc] init];
 }*/

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    currentArtwork = [self.searchResults objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"ShowArtworkDetailView" sender:self];
    
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //NCArtwork * artwork = [self.searchResults objectAtIndex:indexPath.row];
    // 2
    CGSize retval = /**artwork..thumbnail.size.width > 0 ? photo.thumbnail.size : */CGSizeMake(180, 112);
    retval.height += 35; retval.width += 35;
    return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

#pragma mark - SearchForTags
- (void) searchForTags:(NSArray *)tagsId {
    // 1
    
    
    /**
    NSMutableArray * tagIdArray = [[NSMutableArray alloc]init];
    NSMutableArray * refusedTags = [[NSMutableArray alloc]init];
    
    NSDictionary * tagsDic = [[NCArtworkManager sharedManager] getTagsDictionary];
    
    for(NSString * tagName in searchresults){
        
        NSString * tagId =  [tagsDic objectForKey:tagName];
        if (tagId == nil){
            
            [refusedTags addObject:tagName];
            
        }
        else{
            
            [tagIdArray addObject:tagId];
            
        }
        
    }
    
    if([refusedTags count] >0){
        
        NSString * tagNonFoundAlertMessage = [refusedTags description];
        
        UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Tags not found" message: tagNonFoundAlertMessage delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
        
        [someError show];
        
    }
    */
    
    if([tagsId count] >0){
        
        
                
        [[NCArtworkManager sharedManager] getAllArtworksForTags:tagsId withCompletionBlock:^(NSError * error, NSArray * results){
            
            if(results && [results count] > 0){
                
                NSLog(@"Found %d photos matching.", [results count]);
                
                self.searchResults = [[NSMutableArray alloc]initWithArray:results];
                
                [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:self.searchResults];
                
            }
            else{
                
                UIAlertView *someError = [[UIAlertView alloc] initWithTitle: @"Search" message: @"Não há ninjices com essas tags" delegate: self cancelButtonTitle: @"Ok" otherButtonTitles: nil];
                
                [someError show];
                
                self.searchResults = [[NSMutableArray alloc]init];
                
                [self.collectionView reloadData];
                
                NSLog(@"Error searching %@", error.description);
            }
            
        }];
        
        
        
    }
    else {
        
        
        
    }
    
}

#pragma mark - Segue Method
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"ShowArtworkDetailView"])
    {
        // Get reference to the destination view controller
        NCArtworkDetailViewController * advc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [advc setArtwork:currentArtwork];
    }
    
    else if ([[segue identifier] isEqualToString:@"ShowMap"])
    {
        // Get reference to the destination view controller
        NCMapViewController * advc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [advc setArtworkCollection:self.searchResults];
    }

}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

-(void)allArtworkImagesDownloaded{
    
    
    [loadingImage removeFromSuperview];
    [self.collectionView reloadData];
    
}
- (IBAction)selectTags:(id)sender {
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NCCategoryViewController * categoryVC = (NCCategoryViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NCCategoryViewController"];
    
    categoryVC.delegate = self;
    
    categoryPopover = [[UIPopoverController alloc]initWithContentViewController:categoryVC];
    
    [categoryPopover presentPopoverFromRect:CGRectMake(555, 70, 500, 500) inView:self.view permittedArrowDirections: UIPopoverArrowDirectionAny animated:YES];
    
   
    
}

-(void)categorySelected:(NSArray *)array{
    
    
    [self searchForTags:array];
    
    
    [categoryPopover dismissPopoverAnimated:YES];
    
    
}
 -(void) loadNextImages{
     
     firstWork += 6;
     
     
     [[NCArtworkManager sharedManager] getWorksOrderedByDateFrom:firstWork to:7 withCompletionBlock:^(NSError*error, NSArray*results){
         
         
         if(results && [results count] > 0){
             
             NSLog(@"Found %d photos matching.", [results count]);
             
             [self.searchResults addObjectsFromArray:results];
             
             [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:self.searchResults];
             
         }
         else{
             
             NSLog(@"Error searching %@", error.description);
             [loadingImage removeFromSuperview];
         }
         
     }];

     
        
 }

    
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height) {
        
        NSLog(@"Cheguei ao fim");
        // reached the bottom
        [self loadNextImages];
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"loadingGIF" withExtension:@"gif"];
        
        loadingImage = [[UIImageView alloc]initWithFrame:CGRectMake(320,570,  50, 50)];
        
        loadingImage.image = [UIImage animatedImageWithAnimatedGIFURL:url];
        
        [self.view addSubview:loadingImage];

        
    }
}
    

@end
