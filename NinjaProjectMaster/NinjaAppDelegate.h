//
//  NinjaAppDelegate.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/27/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NinjaAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;



@end
