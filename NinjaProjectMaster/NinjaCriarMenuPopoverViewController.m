//
//  NinjaCriarMenuPopoverViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 6/3/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaCriarMenuPopoverViewController.h"
#import "NinjaDetailViewController.h"

@interface NinjaCriarMenuPopoverViewController ()

@end

@implementation NinjaCriarMenuPopoverViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    if ([super initWithStyle:style] != nil) {
        
        CGRect titleRect = CGRectMake(0, 0, 100.0, 44.0);
        
        UILabel *tableTitle = [[UILabel alloc]initWithFrame:titleRect];
        
        tableTitle.textColor = [UIColor whiteColor];
        
        tableTitle.backgroundColor = [UIColor blackColor];
        
        tableTitle.opaque = YES;
        
        tableTitle.font = [UIFont boldSystemFontOfSize:20];
        
        tableTitle.text = @"  Criar Imagem a partir de:";
        
        tableTitle.textAlignment = NSTextAlignmentLeft;
        
        self.tableView.tableHeaderView = tableTitle;
        
        //[self.tableView reloadData];
        
        // Inicializa o Array
        _sourceImageNames = [NSMutableArray array];
        
        // Define o Array de sourceImages
        [_sourceImageNames addObject:@"Câmara Fotográfica"];
        [_sourceImageNames addObject:@"Imagens do Dispositivo"];
        
        // Faz com que a selecção fique persistente
        self.clearsSelectionOnViewWillAppear = YES;
        
        // Calcula a altura que a view deve ter, multiplicando
        // a altura de uma linha pelo total de linhas
        NSInteger rowsCount = [_sourceImageNames count];
        NSInteger singleRowHeight = [self.tableView.delegate tableView:self.tableView
                                               heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        NSInteger totalRowsHeight = rowsCount * singleRowHeight + 44;
        
        // Calcula a largura que a view deverá ter
        // verificando a largura que a string deverá ter
        CGFloat largestLabelWidth = 0;
        for (NSString *sourceImageNames in _sourceImageNames) {
            // Verifica o tamanho do texto utilizando a font default para UITableViewCell's textLabel.
            CGSize labelSize = [sourceImageNames sizeWithFont:[UIFont boldSystemFontOfSize:20.0f]];
            if (labelSize.width > largestLabelWidth) {
                largestLabelWidth = labelSize.width;
            }
        }
        
        // Adiciona padding à largura
        CGFloat popoverWidth = largestLabelWidth + 40;
        
        // Set da propriedade para informar o Popover Container do tamanho da View
        self.contentSizeForViewInPopover = CGSizeMake(popoverWidth, totalRowsHeight);
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_sourceImageNames count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CellCriarMenu";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configurar a Cell
    cell.textLabel.text = [_sourceImageNames objectAtIndex:indexPath.row];
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *selectedSourceImage = [_sourceImageNames objectAtIndex:indexPath.row];
    
        
    // Set da SourceImage baseado no que foi escolhido
    if ([selectedSourceImage isEqualToString:@"Câmara Fotográfica"]) {
        NSLog(@"Câmara Fotográfica");
        
    } else if ([selectedSourceImage isEqualToString:@"Imagens do Dispositivo"]){
        NSLog(@"Imagens do Dispositivo");
    }
    
}

@end
