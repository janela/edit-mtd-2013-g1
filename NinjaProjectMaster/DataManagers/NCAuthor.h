//
//  NSAuthor.h
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 08/06/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NCAuthor : NSObject <NSCoding>

@property NSString * identity;

@property NSString * facebookid;

@property NSString * name;

@property NSMutableArray * obras;

@property UIImage * profilePhoto;


- (id)initFromDictionary:(NSDictionary*) dictionary;

@end
