//
//  EDArtworkManagerDataSource.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCAuthor.h"
#import "NCArtwork.h"

@class NCArtworkManager;

@protocol NCArtworkManagerDataSource <NSObject>





- (void)artworkManager:(NCArtworkManager *)artworkManager
 getAllArtworksForTags:(NSArray *)tags withCompletionBlock:(void (^)(NSError * error, NSArray * artworks))completionBlock;


-(NCArtwork *) getArtworkWithID: (NSNumber *) identity;
           
-(void) artworkManager:(NCArtworkManager*) artworkManager saveArtwork:(UIImage*) image;

-(NSArray*) getAllSavedArtwork;


-(BOOL)artworkManager:(NCArtworkManager *)artworkManager deleteArtwork:(NCArtwork*) artwork fromAuthor:(NCAuthor*) author;


- (void) artworkManager:(NCArtworkManager*)artworkManager
getAllArtworksForUserId:(NSString*)userId withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;

//Tags

-(void) artworkManager:(NCArtworkManager *)artworkManager
getAllArtworksTagswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

-(void) artworkManager:(NCArtworkManager *)artworkManager
  getAllArtworksForTagId: (NSString *) tag withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

-(NSDictionary*) getTagsDictionary;

//Author

-(NCAuthor*) getCurrentAuthor;

-(void) artworkManager:(NCArtworkManager *)artworkManager
getAllAuthorswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

-(void) artworkManager:(NCArtworkManager *)artworkManager
       getAuthorWithId: (NSString*) authorID withCompletionBlock:(void (^)(NSError *, NCAuthor *))completionBlock;

-(void) setCurrentAuthor:(NCAuthor*) author;

-(NCAuthor*) currentAuthor;

//Works

-(void) artworkManager: (NCArtworkManager*) artworkManager getWorksOrderedByDateFrom:(int) firstWork to:(int) lastWork withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

-(void) saveArraysToDocuments;

-(void) artworkManager:(NCArtworkManager *)artworkManager getAllArtworksOrderedByDateWithCompletionBlock:(void (^)(NSError* error, NSArray * artworks))completionBlock;


-(void) artworkManager:(NCArtworkManager *)artworkManager
         getWorkWithId: (NSString*) workID withCompletionBlock:(void (^)(NSError *, NCArtwork *))completionBlock;

-(BOOL) artworkManager: (NCArtworkManager*)artworkManager publishArtWorkWithTitle:(NSString*)title withDescription:(NSString*)description withTags:(NSString*)tags withLocation:(CLLocationCoordinate2D) coordinate withImage:(UIImage*)image withCompletionBlock:(void (^)(NSError *, NSDictionary *))completionBlock;

-(void) artworkManager:(NCArtworkManager *)artworkManager
       getWorksWithIds: (NSArray *) arrayWithIds withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

//Saved Artworks

-(void) artworkManager:(NCArtworkManager*) artworkManager removeArtwork:(UIImage*) image;


//Authentication

-(void) artworkManager:(NCArtworkManager *)artworkManager validateAndCreateUser:(NSString *) fbUserId withCompletionBlock:(void (^)(NSError *, NSDictionary *))completionBlock;


@end
