//
//  EDArtworkManagerDynamicDataSource.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "NCArtworkManagerDynamicDataSource.h"
#import <Parse/Parse.h>
#import "NCFileHandler.h"
#import "EDURLConnectionLoader.h"
#import "CJSONDeserializer.h"


#define kNC_TagsURL @"http://edit-mtd-2013-1s.blissapplications.com/api/tags"
#define kNC_AuthorsURL @"http://edit-mtd-2013-1s.blissapplications.com/api/authors"
#define kNC_WorksURL @"http://edit-mtd-2013-1s.blissapplications.com/api/works"
#define kNC_AuthURL @"http://edit-mtd-2013-1s.blissapplications.com/api/auth"

#define kEDRequestTimeout 60
#define	kEDContentType_MultipartFormDataKey @"Content-Type"
#define kEDMultipartFormDataBoundary @"WebKitFormBoundaryEG0qL3iLd2XGGv3J"
#define kEDContentType_MultipartFormDataValue @"multipart/form-data; boundary=WebKitFormBoundaryEG0qL3iLd2XGGv3J"
#define kEDWorkPostTitleKey @"Title"
#define kEDWorkPostTagsKey @"Tags"
#define kEDWorkPostLatitudeKey @"Latitude"
#define kEDWorkPostLongitudeKey @"Longitude"
#define kEDWorkPostFileKey @"File"
#define kNC_FBAcessTokenHeaderField @"FBAccessToken"

#define kNCArtworkSavedArray @"artworksavedarray.dat"
#define kNCArtworkPublishedArray @"artworkpublishedarray.dat"



@interface NCArtworkManagerDynamicDataSource()
{
    
    
    NSDictionary * facebookUser;
    
    NSMutableArray * savedArtwork;
    
    NSDictionary * artWorkDictionary;
    
    NSArray * publishedArtworkCache;
    
    NSArray * publishedArtworkAllAuthor;
    
    NSArray * arrayWithArtWorkDictionarys;
            
    BOOL publishedArtWorksNeedsToBeUpdated;
    
    NCAuthor * currentAuthor;
    
    NSDictionary * tagsIdDictionary;


    
}

@end


@implementation NCArtworkManagerDynamicDataSource

-(NCArtwork *)getArtworkWithID:(NSNumber *)identity{
    
    return nil;
    
}

-(NCAuthor *)getCurrentAuthor{
    
    return nil;
}


- (BOOL)artworkManager:(NCArtworkManager *)artworkManager deleteArtwork:(NCArtwork *)artwork fromAuthor:(NCAuthor *)author{
    
    return YES;
    
}



#pragma mark Tags

-(void) artworkManager:(NCArtworkManager *)artworkManager
getAllArtworksTagswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock
{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    NSURL *url = [NSURL URLWithString:kNC_TagsURL];
    
    
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    
    
    
    NSURLResponse * response = nil;
    
    NSError *error = nil;
    
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];

    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSISOLatin1StringEncoding];
    
    
    
	NSLog(@"Google HP: %@", responseString);
	
	completionBlock(nil, @[@"Objecto dinamico 1", @"Objecto Dinamico 2"]);
     
    
    
}
- (void)artworkManager:(NCArtworkManager *)artworkManager getAllArtworksForTags:(NSArray *)tags
   withCompletionBlock:(void (^)(NSError * , NSArray *))completionBlock
{
    
    NSMutableArray * arrayOfworks = [[NSMutableArray alloc]init];
    
    __block NSArray * worksArray1;
    
    for (NSString * tagId in tags) {
        
        
        worksArray1 = nil;
        
        [self artworkManager:nil getAllArtworksForTagId:tagId withCompletionBlock:^(NSError* error, NSArray * worksArray){
            
            
            worksArray1 = worksArray;
            
            if(error != nil){
                
                completionBlock(error,nil);
            }
           
                        
        }];
        
        while (worksArray1 == nil){
            
            NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0.1];
            [[NSRunLoop currentRunLoop] runUntilDate:date];
            
        }
        
        [arrayOfworks addObject:[[NSArray alloc]initWithArray:worksArray1]];
        
    

    }
    
    [self artworkManager:nil getWorksWithIds:[self doTheIntersectionOfTheArrays:arrayOfworks] withCompletionBlock:^(NSError * error, NSArray * array){
    
        completionBlock(error,array);
    
    }];
    
    
    
        
    
    
}


-(void) artworkManager:(NCArtworkManager *)artworkManager
  getAllArtworksForTagId: (NSString *) tag withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock
{
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    NSString * stringurl = kNC_TagsURL;
  
    stringurl = [stringurl stringByAppendingString:@"/"];
    
    stringurl = [stringurl stringByAppendingString:tag];
    
    NSURL * url = [NSURL URLWithString:stringurl];
        
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    [request setValue:tag forHTTPHeaderField:@"TagId"];
    
    
    
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
    
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        
        
        
        NSDictionary * dic = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
        NSDictionary * dataDic = [dic objectForKey:@"Data"];
        
        NSArray * array = [dataDic objectForKey:@"Works"];
        
        completionBlock(error,array);
        
        
    };
    
    [loader load];
       
}

#pragma mark Author Requests


-(void) artworkManager:(NCArtworkManager *)artworkManager
getAllAuthorswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    NSURL *url = [NSURL URLWithString:kNC_AuthorsURL];
    
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    
    
    
    NSURLResponse * response = nil;
    
    NSError *error = nil;
    
	NSData *responseData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSISOLatin1StringEncoding];
    
    
    
	NSLog(@"Google HP: %@", responseString);
	
	completionBlock(nil, @[@"Objecto dinamico 1", @"Objecto Dinamico 2"]);

    
    
    
}

-(void) artworkManager:(NCArtworkManager *)artworkManager
       getAuthorWithId: (NSString*) authorID withCompletionBlock:(void (^)(NSError *, NCAuthor *))completionBlock{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    
    NSString * accessToken = [accessTokenData accessToken];
    
    NSString * stringurl = kNC_AuthorsURL;
    
    stringurl = [stringurl stringByAppendingString:@"/"];
    
    stringurl = [stringurl stringByAppendingString:authorID];
    
    NSURL * url = [NSURL URLWithString:stringurl];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    

    
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
    
	loader.completionBlock = ^(NSError *error, NSData * responseData){

        NSDictionary * dictionary = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
       NCAuthor * author = [[NCAuthor alloc] initFromDictionary:[dictionary objectForKey:@"Data"]];
        
        completionBlock(error,author);
        
        
    };
    
    
    [loader load];
        
        
    
}

#pragma mark Works Requests


-(void) artworkManager: (NCArtworkManager*) artworkManager getWorksOrderedByDateFrom:(int) firstWork to:(int) lastWork withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    
        
        [self artworkManager:nil getAllArtworksOrderedByDateWithCompletionBlock:^(NSError * error , NSArray* array ){
            
            if(firstWork ==0){
                publishedArtworkAllAuthor = [[NSArray alloc] initWithArray:array];
            }
            
            NSRange range;
            
            range.location = firstWork;
            
            if([array count] > firstWork){
                
                if([array count] < firstWork + lastWork){
                    
                    range.length = [array count] - firstWork;
                    
                }
                else{
                    
                    range.length = lastWork;
                }
                
                NSIndexSet * setIndexed = [[NSIndexSet alloc] initWithIndexesInRange:range];
                
                NSArray * arraySet = [publishedArtworkAllAuthor objectsAtIndexes:setIndexed];
                completionBlock(error,arraySet );
            }
            else{
               
                
                completionBlock(error,nil);
                
                
                
                
            }
            
            
            
        }];
        
    
    
	    
}

-(void) artworkManager: (NCArtworkManager*) artworkManager getAllArtworksOrderedByDateWithCompletionBlock:(void (^) (NSError*, NSArray*))completionBlock{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    
    NSString * stringurl = kNC_WorksURL;
    
    stringurl = [stringurl stringByAppendingString:@"/all"];
    
    
    
    
    NSURL * url = [NSURL URLWithString:stringurl];
    
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
    
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        
        NSDictionary * dictionary = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
        NSArray * array = [dictionary objectForKey:@"Data"];
        
        NSMutableArray * arrayWithResults = [[NSMutableArray alloc]init];
        
        for (NSDictionary * artworkDic in array) {
            
            [arrayWithResults addObject:[[NCArtwork alloc]initWithDictionary:artworkDic]];
        }
        
        completionBlock(error,arrayWithResults);
        
        
        
    };
    
    [loader load];
    
   

    
    
    
}


-(BOOL) artworkManager: (NCArtworkManager*)artworkManager publishArtWorkWithTitle:(NSString*)title withDescription:(NSString*)description withTags:(NSString*)tags withLocation:(CLLocationCoordinate2D) coordinate withImage:(UIImage*)image withCompletionBlock:(void (^)(NSError *, NSDictionary *))completionBlock
{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];

    
    
    
    NSURL *url = [NSURL URLWithString:kNC_WorksURL];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:kEDRequestTimeout];
	request.HTTPMethod = @"POST";
    
    
    
	[request addValue:accessToken forHTTPHeaderField:@"FBAccessToken"];
	[request addValue:kEDContentType_MultipartFormDataValue
   forHTTPHeaderField:kEDContentType_MultipartFormDataKey];
	
    
    
	NSArray *tagsArray = [tags componentsSeparatedByString:@","];
	
    //	NSString *filePath = [[NSBundle mainBundle] pathForResource:@"Image.jpg" ofType:nil];
    
    NSData * imageData = UIImageJPEGRepresentation(image, 1.0);
	
	NSMutableDictionary *paramsDict = [NSMutableDictionary dictionary];
	[paramsDict setValue:title forKey:kEDWorkPostTitleKey];
	[paramsDict setValue:[NSNumber numberWithDouble:coordinate.latitude] forKey:kEDWorkPostLatitudeKey];
	[paramsDict setValue:[NSNumber numberWithDouble:coordinate.longitude] forKey:kEDWorkPostLongitudeKey];
	[paramsDict setValue:tagsArray forKey:kEDWorkPostTagsKey];
	[paramsDict setValue:imageData forKey:kEDWorkPostFileKey];
	
	NSMutableData *bodyData = [NSMutableData data];
	
	for (NSString *key in paramsDict.allKeys)
	{
		id value = [paramsDict valueForKey:key];
		
		if([value isKindOfClass:[NSData class]])
		{
			NSData *valueData = (NSData*)value;
			NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
			[paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"DSC_0001.jpg\"\r\n",key];
			[paramValue appendFormat:@"Content-Type: image/jpeg\r\n\r\n"];
			
			NSMutableData *paramData = [NSMutableData dataWithData:[paramValue dataUsingEncoding:NSUTF8StringEncoding]];
			[paramData appendData:valueData];
			[paramData appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
			[bodyData appendData:paramData];
		}
		else if([value isKindOfClass:[NSArray class]])
		{
			NSArray *valueArray = (NSArray*)value;
			NSString *valueString = [valueArray componentsJoinedByString:@","];
			NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
			[paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
			[paramValue appendFormat:@"%@\r\n",valueString];
			NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
			[bodyData appendData:paramData];
		}
		else if([value isKindOfClass:[NSNumber class]])
		{
			NSNumber *valueNumber = (NSNumber*)value;
			NSString *valueString = [valueNumber stringValue];
			NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
			[paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
			[paramValue appendFormat:@"%@\r\n",valueString];
			NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
			[bodyData appendData:paramData];
		}
		else
		{
			NSMutableString *paramValue = [NSMutableString stringWithFormat:@"--%@\r\n",kEDMultipartFormDataBoundary];
			[paramValue appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
			[paramValue appendFormat:@"%@\r\n",value];
			NSLog(@"Param Data:\n%@",paramValue);
			NSData *paramData = [paramValue dataUsingEncoding:NSUTF8StringEncoding];
			[bodyData appendData:paramData];
		}
	}
	NSData *boundary = [[NSString stringWithFormat:@"--%@--\r\n",kEDMultipartFormDataBoundary] dataUsingEncoding:NSUTF8StringEncoding];
	[bodyData appendData:boundary];
	
	
	[request setHTTPBody:bodyData];
	
	NSString *bodyDataFile = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"bodyData.txt"];
	NSLog(@"Body is at: %@",bodyDataFile);
	[bodyData writeToFile:bodyDataFile atomically:YES];
	
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
	loader.progressBlock = ^(CGFloat progress){
		NSLog(@"Progress of upload: %f %%", progress * 100);
	};
	loader.completionBlock = ^(NSError *error, NSData * responseData){
        
        NSDictionary *  responseDictionary = [[CJSONDeserializer deserializer]deserializeAsDictionary:responseData error:&error];
        
        NSDictionary * dataDic = [responseDictionary objectForKey:@"Data"];
        
		NSLog(@"Response: %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
        
        publishedArtWorksNeedsToBeUpdated = YES;
        
        completionBlock(error,dataDic);
	};
	[loader load];
    
    
    return YES;
}


-(void) artworkManager:(NCArtworkManager *)artworkManager
       getWorkWithId: (NSString*) workID withCompletionBlock:(void (^)(NSError *, NCArtwork *))completionBlock{
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    NSString * stringurl = kNC_WorksURL;
    
    stringurl = [stringurl stringByAppendingString:@"/"];
    
    stringurl = [stringurl stringByAppendingString:workID];
    
    NSURL * url = [NSURL URLWithString:stringurl];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    
    
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
	        
	loader.completionBlock = ^(NSError *error, NSData * responseData){
		
        NSDictionary * responseDictionary = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
        artWorkDictionary = [responseDictionary objectForKey:@"Data"];
        
        NSLog(@"%@", artWorkDictionary);
        
        NCArtwork * artwork = [[NCArtwork alloc] initWithDictionary:artWorkDictionary];
        
        
        completionBlock(nil, artwork);

        
        
	};
	[loader load];
    
    
       
}

-(void) artworkManager:(NCArtworkManager *)artworkManager
         getWorksWithIds: (NSArray *) arrayWithIds withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    
    NSString * stringurl = kNC_WorksURL;
    
    stringurl = [stringurl stringByAppendingString:@"/byids?"];
    
    for (int i =0; i < arrayWithIds.count; i++) {
         stringurl = [stringurl stringByAppendingFormat:@"listOfIds=%@&", [arrayWithIds objectAtIndex:i] ];
    }
    
   
    
    NSURL * url = [NSURL URLWithString:stringurl];
    
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:accessToken forHTTPHeaderField: kNC_FBAcessTokenHeaderField];
    
    
    EDURLConnectionLoader *loader = [[EDURLConnectionLoader alloc] initWithRequest:request];
	
    
	loader.completionBlock = ^(NSError *error, NSData * responseData){
		
        NSDictionary * responseDictionary = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
        NSArray *array = [responseDictionary objectForKey:@"Data"];
        
        NSMutableArray * arrayWithWorks = [[NSMutableArray alloc] init];
        
        for(NSDictionary * art in array){
            
            [arrayWithWorks addObject:[[NCArtwork alloc] initWithDictionary:art]];
        }
        
               
        completionBlock(error, arrayWithWorks);
      
    };
	[loader load];
    
    
    
}

- (void)artworkManager:(NCArtworkManager *)artworkManager
getAllArtworksForUserId:(NSString *)userId
   withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock
{
    
    if([userId isEqualToString:currentAuthor.identity] && !publishedArtWorksNeedsToBeUpdated){
        
        completionBlock(nil,publishedArtworkCache);
    }
    
    else{
        
        [self artworkManager:artworkManager getAuthorWithId:userId withCompletionBlock:^(NSError*error, NCAuthor * author){
       
        
            [self artworkManager:artworkManager getWorksWithIds:author.obras withCompletionBlock:^(NSError * error2, NSArray*artworkArray){
                if([userId isEqualToString:currentAuthor.identity]){
                    publishedArtWorksNeedsToBeUpdated = NO;
                    publishedArtworkCache = [NSArray arrayWithArray:artworkArray];
                    
                }
                
                completionBlock(error2,artworkArray);
            
            }];
       
        }];
        
    }
    
}


#pragma mark Authentication

-(void) artworkManager:(NCArtworkManager *)artworkManager validateAndCreateUser:(NSString *) fbUserId withCompletionBlock:(void (^)(NSError *, NSDictionary *))completionBlock{
    
    FBAccessTokenData * accessTokenData = [[FBSession activeSession] accessTokenData];;
    NSString * accessToken = [accessTokenData accessToken];
    
    NSString * stringurl = kNC_AuthURL;
    
    NSURL * url = [NSURL URLWithString:stringurl];
    
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:url];
    
    NSString * value = [NSString stringWithFormat:@"{\"FBAccessToken\": \"%@\",\"FBUserId\": \"%@\"}",accessToken,fbUserId ];
    
    [request setHTTPMethod:@"POST"];
    
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[value dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    EDURLConnectionLoader * urlConnectionLoader = [[EDURLConnectionLoader alloc] initWithRequest:request];
    
    
    urlConnectionLoader.completionBlock = ^(NSError *error, NSData * responseData){
		
        NSDictionary * responseDictionary = [[CJSONDeserializer deserializer] deserializeAsDictionary:responseData error:&error];
        
        NSDictionary * dataDictionary = [responseDictionary objectForKey:@"Data"];
        
        completionBlock(error, dataDictionary);
        
                
	};
    
	[urlConnectionLoader load];
    
    
	
	
	
    
}




#pragma mark Object Init

- (id)init
{
    self = [super init];
    if (self) {
        
        if([NCFileHandler fileExists:kNCArtworkSavedArray]){
            
            
            savedArtwork = [NSMutableArray arrayWithArray:[NCFileHandler loadArrayFromDocuments:kNCArtworkSavedArray]];
            
            
        }
        else{
            savedArtwork = [[NSMutableArray alloc]init];
        }
        
        if([NCFileHandler fileExists:kNCArtworkPublishedArray]){
            
            
            publishedArtworkCache = [NSMutableArray arrayWithArray:[NCFileHandler loadArrayFromDocuments:kNCArtworkPublishedArray]];
            publishedArtWorksNeedsToBeUpdated = YES;
            
        }
        else{
            publishedArtWorksNeedsToBeUpdated = YES;
        }
        
        tagsIdDictionary = [self getTagsDictionary];
        
        
        }
    return self;
}


-(void) saveArraysToDocuments{
    
    
    [NCFileHandler  writeArrayToDocuments:savedArtwork toFile:kNCArtworkSavedArray];
    [NCFileHandler writeArrayToDocuments:publishedArtworkCache toFile:kNCArtworkPublishedArray];
    
    
    
}




-(NSArray*) getAllSavedArtwork{
    
    return [[NSArray alloc]initWithArray:savedArtwork];
    
}

    

-(void) setCurrentAuthor:(NCAuthor*) author{
    
    currentAuthor = author;
    
}

-(NCAuthor*) currentAuthor{
    
    return currentAuthor;
    
}

-(NSDictionary*) getTagsDictionary{
    
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc]init];
    
    [dic setObject:@"1" forKey:@"abstract"];
    [dic setObject:@"2" forKey:@"urban"];
    [dic setObject:@"3" forKey:@"nature"];
    [dic setObject:@"4" forKey:@"fashion"];
    [dic setObject:@"5" forKey:@"people"];
    [dic setObject:@"6" forKey:@"family"];
    [dic setObject:@"7" forKey:@"art"];
    [dic setObject:@"8" forKey:@"cartoon"];
    [dic setObject:@"9" forKey:@"photography"];
    [dic setObject:@"10" forKey:@"other"];
    
    return dic;
    
}

-(NSArray *) doTheIntersectionOfTheArrays:(NSArray*) array{

    
    NSMutableSet * artworkSet = [[NSMutableSet alloc]init];
    
    NSSet * set;
    
    BOOL firstSearch = 1;
    
    for (NSArray * arrayWithIds in array) {
        set = [[NSSet alloc]initWithArray: arrayWithIds];
        
        if(set != nil){
            
            if(firstSearch){
                [artworkSet unionSet:set];
                firstSearch = 0;
            }
            else{
                [artworkSet intersectSet:set];
            }
        }
    }
    
    
    
    
    
    NSLog(@"Artworkset %@",artworkSet);
    
    return [artworkSet allObjects];
    
    
    
    
}

-(void) artworkManager:(NCArtworkManager*) artworkManager saveArtwork:(UIImage*) image{
    
    [savedArtwork addObject:image];
    
    
}

-(void) artworkManager:(NCArtworkManager*) artworkManager removeArtwork:(UIImage*) image{
    
    [savedArtwork removeObject:image];
    
    
}

@end
