//
//  EDArtworkManager.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "NCArtworkManager.h"
#import "NCArtworkManagerStubDataSource.h"
#import "NCArtworkManagerDynamicDataSource.h"
#import "NSThread+MCSMAdditions.h"

#define kEDArtworkManager_UseStubDataSource 0




@implementation NCArtworkManager

-(void) publishArtWorkWithTitle:(NSString*)title withDescription:(NSString*)description withTags:(NSString*)tags withLocation:(CLLocationCoordinate2D) coordinate withImage:(UIImage*)image withCompletionBlock:(void (^)(NSError *, NSDictionary  *))completionBlock{
    [NSThread MCSM_performBlockInBackground:^{
        [_dataSource artworkManager:self publishArtWorkWithTitle:title withDescription:description withTags:tags withLocation:coordinate withImage:image withCompletionBlock:^(NSError*error ,NSDictionary* dictionary){
       
            [NSThread MCSM_performBlockOnMainThread:^{
                completionBlock(error,dictionary);
            }];

        
        }];
    
    }];
     
     
}

-(void)getAllArtworksTagswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self getAllArtworksTagswithCompletionBlock:^(NSError*error, NSArray * artworks)  {
            [NSThread MCSM_performBlockOnMainThread:^{
                completionBlock(error,artworks);
            }];
            
        }];
    }];
    
}
#pragma mark Tags

-(NSDictionary*) getTagsDictionary{
    
   return  [_dataSource getTagsDictionary];
}

-(void)getAllArtworksForTagId:(NSString*) tag withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self getAllArtworksForTagId:tag withCompletionBlock:^(NSError*error, NSArray * artworks)  {
            [NSThread MCSM_performBlockOnMainThread:^{
                completionBlock(error,artworks);
            }];
            
        }];
    }];
    
}

- (void)getAllArtworksForTags:(NSArray *)tags
   withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    
    [NSThread MCSM_performBlockInBackground:^{
		[_dataSource artworkManager:self
            getAllArtworksForTags:tags
                withCompletionBlock:^(NSError *error, NSArray *artworks) {
                    [NSThread MCSM_performBlockOnMainThread:^{
                        completionBlock(error,artworks);
                    }];
                }];
	}];

    
}

- (void)getAllArtworksForUserId:(NSString*)userId
						withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock
{
	[NSThread MCSM_performBlockInBackground:^{
		[_dataSource artworkManager:self
				getAllArtworksForUserId:userId
						withCompletionBlock:^(NSError *error, NSArray *artworks) {
							[NSThread MCSM_performBlockOnMainThread:^{
										completionBlock(error,artworks);
							}];
						}];
	}];
}

- (void) getAllArtworksOrderedByDateWithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self
            getAllArtworksOrderedByDateWithCompletionBlock:^(NSError*error, NSArray * artworks)  {
                [NSThread MCSM_performBlockOnMainThread:^{
                    completionBlock(error,artworks);
                }];
            
             }];
        
        
        
    }];
    
    
}



-(BOOL) deleteArtwork:(NCArtwork*) artwork fromAuthor:(NCAuthor*) author{
    
    
    return [_dataSource artworkManager:self deleteArtwork:artwork fromAuthor:author];
    
    
}
-(void) saveArtwork: (UIImage*)image{
    
    [_dataSource artworkManager:self saveArtwork:image];
    
}

- (NSArray *)getAllSavedArtwork{
    
    return [_dataSource getAllSavedArtwork];
}




#pragma mark Authors

-(NCAuthor*) currentAuthor{
    
    return [_dataSource currentAuthor];
    
}

-(void) setCurrentAuthor:(NCAuthor*) author{
    
    [_dataSource setCurrentAuthor:author];
    
}


-(void) getAllAuthorswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self
            getAllAuthorswithCompletionBlock:^(NSError*error, NSArray * artworks)  {
                [NSThread MCSM_performBlockOnMainThread:^{
                    completionBlock(error,artworks);
            }];
    
        }];
    }];
    
    
}

-(void)getAuthorWithID:(NSString*) authorId withCompletionBlock:(void (^)(NSError *, NCAuthor *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self
                    getAuthorWithId:authorId withCompletionBlock:^(NSError*error, NCAuthor * author)  {
                    [NSThread MCSM_performBlockOnMainThread:^{
                        completionBlock(error,author);
                    }];
                
            }];
    }];

    
    
}

#pragma mark Works

- (void)getWorksOrderedByDateFrom:(int)firstWork to:(int)lastWork withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    
    [NSThread MCSM_performBlockInBackground:^{
        
        
        [_dataSource artworkManager:self getWorksOrderedByDateFrom:firstWork to:lastWork withCompletionBlock:^(NSError*error,NSArray * array){
            
            [NSThread MCSM_performBlockOnMainThread:^{
                completionBlock(error,array);
            }];
            
        }];
        
        
    }];
    
}

-(void) getWorkWithId: (NSString*) workID withCompletionBlock:(void (^)(NSError *, NCArtwork *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self
                      getWorkWithId:workID withCompletionBlock:^(NSError*error, NCArtwork * artwork)  {
                        [NSThread MCSM_performBlockOnMainThread:^{
                            completionBlock(error,artwork);
                        }];
                        
                    }];
    }];
    
    
}

-(void) getWorksWithIds: (NSArray *) arrayWithIds withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock{
    
    [NSThread MCSM_performBlockInBackground:^{
        
        [_dataSource artworkManager:self
                      getWorksWithIds: arrayWithIds withCompletionBlock:^(NSError*error, NSArray * worksArray)  {
                          [NSThread MCSM_performBlockOnMainThread:^{
                              completionBlock(error,worksArray);
                          }];
                          
                      }];
    }];

    
    
    
}


#pragma mark Auth

-(void) validateAndCreateUser:(NSString *) fbUserId withCompletionBlock:(void (^)(NSError *, NSDictionary* userDictionary))completionBlock{
    
    
    [_dataSource artworkManager:self validateAndCreateUser:fbUserId withCompletionBlock:^(NSError*error, NSDictionary* userDictionary )  {
        
            completionBlock(error,userDictionary);
        }];

    
}


#pragma mark Object Lifecycle

- (id)init
{
	self = [super init];
	if (self) {
		if(kEDArtworkManager_UseStubDataSource){
			//_dataSource = [[NCArtworkManagerStubDataSource alloc] init];
		}else {
			_dataSource = [[NCArtworkManagerDynamicDataSource alloc] init];
			//NSAssert(false, @"No known implementation for EDArtworkManagerDataSource.");
		}
	}
	return self;
}

static NCArtworkManager* _sharedManager;

+ (NCArtworkManager *)sharedManager{
	if(!_sharedManager){
		_sharedManager = [[NCArtworkManager alloc] init];
	}
	return _sharedManager;
}

-(void) saveArraysToDocuments{
    
    [_dataSource saveArraysToDocuments];
    
}

-(void) removeSavedArtwork:(UIImage*) image{
    
    [_dataSource artworkManager:self saveArtwork:image];
    
}

@end















