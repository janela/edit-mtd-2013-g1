//
//  EDArtworkManagerStubDataSource.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "NCArtworkManagerStubDataSource.h"
#import "NCFileHandler.h"

//-------------------File Keys ------------------------------------------------
#define kNCArtworkManager_StubArtworksPLIST @"artworkList.plist"
#define kNCArtworkManager_StubTagsPLIST @"tagList.plist"
#define kNCArtworkManager_StubAuthorPLIST @"authorList.plist"
#define kNCArtworkManager_StubSavedArtworkPLIST @"savedArtworkList.plist"

//-------------------Artwork propertys keys------------------------------------
#define kNCArtwork_DictionaryKey_Title @"title"
#define kNCArtwork_DictionaryKey_ImageURL @"imagefilename"
#define kNCArtwork_DictionaryKey_Longitude @"longitude"
#define kNCArtwork_DictionaryKey_Latitude @"latitude"
#define KNCArtwork_DictionaryKey_Tags @"tags"
#define kNCArtwork_DictionaryKey_Description @"description"
#define kNCArtwork_DictionaryKey_Longitude @"longitude"
#define kNCArtwork_DictionaryKey_AuthorID @"authorID"
#define kNCArtwork_DictionaryKey_Id @"id"
#define kNCArtwork_DictionaryKey_Date @"date"
#define kNCArtwork_DictionaryKey_Author @"authorID"
//---------------------Author---------------------------------------------------
#define kNCAuthor_DictionaryKey_Name @"name"
#define kNCAuthor_DictionaryKey_Id @"id"
#define kNCAuthor_DictionaryKey_Facebook @"facebookid"
#define kNCAuthor_DictionaryKey_Works @"works"
#define kNCAuthor_DictionaryKey_Photofilename @"photofilename"






@interface NCArtworkManagerStubDataSource()
{
    
    NSMutableDictionary * tagDictionary;
    
    NSMutableDictionary * objectDictionary;
    
    NSMutableDictionary * authorDictionary;
    
    NSMutableArray * savedArtwork;
    
    NSNumber * highestArtworkIdentity;
    
    
}


@end



@implementation NCArtworkManagerStubDataSource

- (id)init
{
    self = [super init];
    if (self) {
        
        NSString *authorPath = [[NSBundle mainBundle] pathForResource:kNCArtworkManager_StubAuthorPLIST ofType:nil];

        
        NSArray *authorStub = [NSArray arrayWithContentsOfFile:authorPath];
        
        authorDictionary = [self parseAuthorsFromDictionaryArray:authorStub];
        
        NSString *finalPath = [[NSBundle mainBundle] pathForResource:kNCArtworkManager_StubArtworksPLIST ofType:nil];
        
        NSArray *artworksStub = [NSArray arrayWithContentsOfFile:finalPath];
        objectDictionary = [self parseObjectsFromDictionariesArray:artworksStub];
        
        NSString *tagsListPath = [[NSBundle mainBundle] pathForResource:kNCArtworkManager_StubTagsPLIST ofType:nil];
        
        NSDictionary * tagDictionaryTemp = [NSMutableDictionary dictionaryWithContentsOfFile:tagsListPath];
       
        
        tagDictionary = [self parseSetsFromDictionaryArrays:tagDictionaryTemp];
        
        NSString *savedArtworkPath = [[NSBundle mainBundle] pathForResource:kNCArtworkManager_StubSavedArtworkPLIST ofType:nil];
        
        NSArray * savedArtworkTemp = [NSMutableArray arrayWithContentsOfFile:savedArtworkPath];
        
        savedArtwork = [self parseImagesFromArray:savedArtworkTemp];
        
        //Resolver este error. o Saved Artwork não está a ser inicializado.

    }
    return self;
}



#pragma mark NCArtworkManager Protocol Methods

-(BOOL) artworkManager: (NCArtworkManager*)artworkManager publishArtWorkWithTitle:(NSString*)title withDescription:(NSString*)description withTags:(NSString*)tags withLocation:(CLLocationCoordinate2D) coordinate withImage:(UIImage*)image{
    
    
    NCArtwork * artwork = [[NCArtwork alloc]init];
    
    artwork.title = title;
    artwork.description = description;
    artwork.tags  =  [tags componentsSeparatedByString:@" "];
    artwork.coordinate = coordinate;
    
    artwork.image= image;
    
    artwork.author = [authorDictionary objectForKey:[NSNumber numberWithInt:1]];
    
    artwork.publicationDate = [NSDate date];
    
    
    artwork.identity = [self getNewArtworkIdentity];
    
    return [self publishArtWork:artwork];
    
}


- (void)artworkManager:(NCArtworkManager *)artworkManager
getAllArtworksForUserId:(NSString *)userId
	 withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock
{
	
    
    
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setNumberStyle:NSNumberFormatterNoStyle];
    
    NSNumber * number = [f numberFromString:userId];
    
    NCAuthor * author = [authorDictionary objectForKey:number];
    
    NCArtwork * artwork;
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    
    for ( NSNumber * identity in author.obras){
        
        artwork = [objectDictionary objectForKey:identity];
        
        [array addObject:artwork];
        
    }
    
    NSError* error = nil;
    completionBlock(error,array);
}

-(BOOL) publishArtWork:(NCArtwork*) artwork {
    
    NSMutableSet * photoSet;
    
    for ( NSString * tag in artwork.tags){
        
        photoSet = [tagDictionary objectForKey:tag];
        
        if(photoSet != nil){
            
            [photoSet addObject:artwork.identity];
        }
        else {
            
            photoSet = [[NSMutableSet alloc] init];
            [photoSet addObject:artwork.identity];
            [tagDictionary setObject:photoSet forKey:tag];
        }
        
    };
    
    [objectDictionary setObject:artwork forKey:artwork.identity];
    
    NSMutableArray * array = artwork.author.obras;
    [array addObject:artwork.identity];
    
    
    highestArtworkIdentity = artwork.identity;
    
    return true;
    
}

-(BOOL)artworkManager:(NCArtworkManager *)artworkManager deleteArtwork:(NCArtwork*) artwork fromAuthor:(NCAuthor*) author;
{
    
    [objectDictionary removeObjectForKey:artwork.identity];
    NSMutableSet * set;
    
    for (NSString*tag in artwork.tags) {
        set= [tagDictionary objectForKey:tag];
        [set removeObject:artwork.identity];
    }
    
    [author.obras removeObject:artwork.identity];
    

    return YES;
    
}

- (void)artworkManager:(NCArtworkManager *)artworkManager getAllArtworksForTags:(NSArray *)tags
   withCompletionBlock:(void (^)(NSError * , NSArray *))completionBlock
{
    
    NSError* error = nil;
    
    NSMutableSet * artworkSet = [[NSMutableSet alloc]init];
    
    NSSet * set;
    
    BOOL firstSearch = 1;
    
    for (NSString * tag in tags) {
        set = [tagDictionary objectForKey:tag];
        
        if(set != nil){
            
            if(firstSearch){
                [artworkSet unionSet:set];
                firstSearch = 0;
            }
            else{
                [artworkSet intersectSet:set];
            }
        }
    }
    
    
    NSLog(@"Artworkset %@",artworkSet);
    
    NSMutableArray * arrayWithSearchResults = [[NSMutableArray alloc] init];
    
    NCArtwork * artwork;
    
    for (NSNumber * identity in artworkSet){
        
         artwork = [objectDictionary objectForKey:identity];
        [arrayWithSearchResults addObject:artwork];
        
        
    }
    
    
    completionBlock(error,arrayWithSearchResults);
}


-(NCArtwork *) getArtworkWithID: (NSNumber *) identity{
    
    
    return [objectDictionary objectForKey:identity];
    
}

-(void) getAllArtworksForAuthor:(NCAuthor *) author
              withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock
{
    NSError * error = nil;
    
    NSMutableArray * array = [[NSMutableArray alloc]init];
    
    for (NSNumber*number in author.obras) {
        [array addObject:[objectDictionary objectForKey:number]];
    }
    
    completionBlock(error,array);
    
}


-(void) artworkManager:(NCArtworkManager *)artworkManager getAllArtworksOrderedByDateWithCompletionBlock:(void (^)(NSError* error, NSArray * artworks))completionBlock{
    
    
    NSError * error = nil;
    
    NSArray * array = [objectDictionary allValues];
    
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"publicationDate"
                                                  ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [array sortedArrayUsingDescriptors:sortDescriptors];
    
    completionBlock(error,sortedArray);
    
    
}


#pragma mark Parsing methods


-(NSMutableArray*) parseImagesFromArray:(NSArray*) array{
    
    
    NSMutableArray * arrayOfImages = [[NSMutableArray alloc]init];
    
    NSString * filenamePath;
    
    UIImage * image;
    
    for (NSString * string in array){
        
       filenamePath = [[NSBundle mainBundle] pathForResource:string ofType:nil];
        
        image = [UIImage imageWithContentsOfFile:filenamePath];
        
        [arrayOfImages addObject:image];
        
    }
    
    
    return arrayOfImages;
}


- (NSMutableDictionary*) parseObjectsFromDictionariesArray:(NSArray*)dictionariesArray{
	
    NSMutableArray *resultingArray = [NSMutableArray arrayWithCapacity:dictionariesArray.count];
	NSMutableDictionary * objectDictionaryTemp = [[NSMutableDictionary alloc]init];
    
    
	for (NSDictionary *dictionary in dictionariesArray) {
        [resultingArray addObject:[self parseArtworkFromDictionary:dictionary]];
	}
    
    //NSLog(@"%@", resultingArray);
    
    for (NCArtwork * artwork in resultingArray){
        
        
       // NSLog(@"Artwork: %@,%@", artwork.title, artwork.identity);
        [objectDictionaryTemp setObject:artwork forKey:artwork.identity];
        highestArtworkIdentity = artwork.identity;
        
    }
    
    
    
	return objectDictionaryTemp;
}



- (NCArtwork*) parseArtworkFromDictionary:(NSDictionary*)dictionary  {
	
    NCArtwork *artwork = [[NCArtwork alloc]init];
	artwork.title = [dictionary objectForKey:kNCArtwork_DictionaryKey_Title];
	artwork.image = [UIImage imageNamed:[dictionary objectForKey:kNCArtwork_DictionaryKey_ImageURL]];
    artwork.tags = [dictionary objectForKey:KNCArtwork_DictionaryKey_Tags];
    artwork.description = [dictionary objectForKey:kNCArtwork_DictionaryKey_Description];
    artwork.identity = [dictionary objectForKey:kNCArtwork_DictionaryKey_Id];
    
    NSString * latitude = [dictionary objectForKey:kNCArtwork_DictionaryKey_Latitude];
    NSString * longitude = [dictionary objectForKey:kNCArtwork_DictionaryKey_Longitude];
    
    double latitudeDouble = [latitude doubleValue];
    double longitudeDouble = [longitude doubleValue];
    
    
    artwork.coordinate = CLLocationCoordinate2DMake(latitudeDouble , longitudeDouble );
    artwork.author = [authorDictionary objectForKey: [dictionary objectForKey:kNCArtwork_DictionaryKey_Author]];
    artwork.publicationDate = [dictionary objectForKey:kNCArtwork_DictionaryKey_Date];
    
    
	return artwork;
}

-(NSMutableDictionary*) parseSetsFromDictionaryArrays:(NSDictionary*) tagDictionaryTemp{
    

    NSArray * array = [tagDictionaryTemp allKeys];
    NSArray * arrayOfArrays;
    
    
    NSMutableDictionary * dictionary = [[NSMutableDictionary alloc]init];
    
    for (NSString * string in array){
        
        arrayOfArrays = [tagDictionaryTemp objectForKey:string];
        [dictionary setValue:[NSMutableSet setWithArray:arrayOfArrays] forKey:string];
        
        
    }
    
    return dictionary;
    
}

-(NSMutableDictionary*) parseAuthorsFromDictionaryArray: (NSArray*) array{
    
    
    NCAuthor * author ;
    NSMutableDictionary * authors = [[NSMutableDictionary alloc]init];
  
    
    for (NSDictionary * dic in array){
       
        author = [[NCAuthor alloc]init];
        author.name = [dic objectForKey:kNCAuthor_DictionaryKey_Name];
        author.identity  = [dic objectForKey:kNCAuthor_DictionaryKey_Id];
        author.facebookid = [dic objectForKey:kNCAuthor_DictionaryKey_Facebook];
        
        NSMutableArray * array = [NSMutableArray arrayWithArray:[dic objectForKey:kNCAuthor_DictionaryKey_Works]];
        author.obras = array;
        [authors setObject:author forKey:author.identity];
    
        author.profilePhoto = [UIImage imageNamed:[dic objectForKey:kNCAuthor_DictionaryKey_Photofilename]];
    }
    
    return authors;
}




-(void) artworkManager:(NCArtworkManager*) artworkManager saveArtwork:(UIImage*) image{
    
    
    //Arranjar um nome
    
    NSString * filename = @"image";
    
    do {
        filename = [filename stringByAppendingString:[self genRandStringLength:4]];
        filename = [filename stringByAppendingString:@".jpg"];
        
    } while ([NCFileHandler fileExists:filename]);
    
    
    
    [NCFileHandler writeImageToDocuments:image withName:filename ];
    
    [savedArtwork addObject:image];
    
    if([NCFileHandler fileExists:filename])
        NSLog(@"Ficheiro existe");
    
    NSLog(@"%@", savedArtwork);
    
}

-(NSString*) saveImage:(UIImage*)image{
    
    NSString * filename = @"image";
    
    do {
        filename = [filename stringByAppendingString:[self genRandStringLength:4]];
        filename = [filename stringByAppendingString:@".jpg"];
        
    } while ([NCFileHandler fileExists:filename]);
    
    
    
    [NCFileHandler writeImageToDocuments:image withName:filename ];
    
    if([NCFileHandler fileExists:filename])
        NSLog(@"Ficheiro existe: %@",filename);

    return filename;
    
    
    
}


-(NSString *) genRandStringLength: (int) len {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random() % [letters length]]];
    }
    
    return randomString;
}

-(NSArray*) getAllSavedArtwork{
    
    return [[NSArray alloc]initWithArray:savedArtwork];
    
}

-(NSNumber*) getNewArtworkIdentity{
    
    NSNumber * number = highestArtworkIdentity;
    
    int newIdentity = number.intValue +1;
    
    return [NSNumber numberWithInt:newIdentity];
    
    
    
}


@end
