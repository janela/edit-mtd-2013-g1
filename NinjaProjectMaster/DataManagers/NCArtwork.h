//
//  EDArtwork.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "NCAuthor.h"
#import <CoreLocation/CoreLocation.h>

@interface NCArtwork : NSObject <MKAnnotation,NSCoding>

@property (strong, nonatomic) NCAuthor * author;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSDate * publicationDate;
@property (strong, nonatomic) NSArray * tags;
@property (strong, nonatomic) NSNumber * identity;
@property CLLocationCoordinate2D coordinate;

@property (strong, nonatomic) UIImage *image;

@property (strong, nonatomic) NSString * imageUrl;



- (NSComparisonResult)compare:(NCArtwork *)otherObject;

- (id)initWithDictionary:(NSDictionary*) artWorkDictionary;


@end
