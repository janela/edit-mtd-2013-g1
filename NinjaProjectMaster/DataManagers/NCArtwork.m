//
//  EDArtwork.m
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import "NCArtwork.h"
#import "EDURLConnectionLoader.h"

@implementation NCArtwork

- (id)initWithDictionary:(NSDictionary*) artWorkDictionary
{
    self = [super init];
    if (self) {
        
        long identity = [[artWorkDictionary objectForKey:@"Id"]longValue];
        
        self.identity = [NSNumber numberWithLong:identity];
        
        double latitude = [[artWorkDictionary objectForKey:@"Latitude"] doubleValue];
        double longitude = [[artWorkDictionary objectForKey:@"Longitude"] doubleValue];
        
        self.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        
        self.title = [artWorkDictionary objectForKey:@"Title"];
        
        self.tags = [artWorkDictionary objectForKey:@"Tags"];
        
        NSString * dateString = [artWorkDictionary objectForKey:@"CreationDate"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        
        self.publicationDate = [dateFormatter dateFromString:dateString];
        
        self.imageUrl = [artWorkDictionary objectForKey:@"File"];
        
        self.author = [[NCAuthor alloc]init];
        
        self.author.facebookid = [artWorkDictionary objectForKey:@"AuthorFBID"];
        
        self.author.identity = [NSString stringWithFormat:@"%@",[artWorkDictionary objectForKey:@"AuthorId"] ];
        
        
        
        
    }
    return self;
}
-(NSString *)description{
    
    return [_title stringByAppendingString:[_identity description]];
}

- (NSComparisonResult)compare:(NCArtwork *)otherObject {
    
    return [self.publicationDate compare:otherObject.publicationDate];

}



-(NSString*) subtitle{
    
    return self.description;
    
    
    
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    [aCoder encodeObject:self.identity forKey:@"Id"];
    [aCoder encodeObject:self.description forKey:@"Descriptions"];
    [aCoder encodeObject:self.image forKey:@"Image"];
    [aCoder encodeObject:self.imageUrl forKey:@"ImageURL"];
    [aCoder encodeObject:self.publicationDate forKey:@"PublicationDate"];
    [aCoder encodeObject:self.title forKey:@"Title"];
    [aCoder encodeObject:self.tags forKey:@"Tags"];
    
    [aCoder encodeDouble:self.coordinate.latitude forKey:@"Latitude"];
    [aCoder encodeDouble:self.coordinate.longitude forKey:@"Longitude"];
    
    [aCoder encodeObject:self.author forKey:@"Author"];
    
    
    
    
    
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    
    NCArtwork * artwork = [[NCArtwork alloc]init];
    
    artwork.identity = [aDecoder decodeObjectForKey:@"Id"];
    artwork.description = [aDecoder decodeObjectForKey:@"Description"];
    artwork.image = [aDecoder decodeObjectForKey:@"Image"];
    artwork.imageUrl =[aDecoder decodeObjectForKey:@"ImageURL"];
    artwork.publicationDate = [aDecoder decodeObjectForKey:@"PublicationDate"];
    artwork.title = [aDecoder decodeObjectForKey:@"Title"];
    artwork.tags = [aDecoder decodeObjectForKey:@"Tags"];
    
    
    
    artwork.coordinate = CLLocationCoordinate2DMake([aDecoder decodeDoubleForKey:@"Latitude"],
                                                 [aDecoder decodeDoubleForKey:@"Longitude"]);
    
    
    artwork.author = [aDecoder decodeObjectForKey:@"Author"];
    
    return artwork;
    
    
}


@end
