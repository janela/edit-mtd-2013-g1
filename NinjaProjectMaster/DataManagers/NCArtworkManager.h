//
//  EDArtworkManager.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCArtworkManagerDataSource.h"
#import "NCArtwork.h"
#import "NCAuthor.h"

@interface NCArtworkManager : NSObject
{
	id<NCArtworkManagerDataSource> _dataSource;
    
    }

@property (strong,nonatomic) NCAuthor  * currentAuthor;

@property BOOL workPublished;


-(void)getAllArtworksForTagId:(NSString*) tag withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

- (void)getAllArtworksForUserId:(NSString*)userId
		  withCompletionBlock:(void(^)(NSError* error, NSArray* artworks))completionBlock;

- (void)getAllArtworksForTags:(NSArray *)tags
          withCompletionBlock:(void (^)(NSError* error, NSArray * artworks))completionBlock;




-(void) saveArtwork: (UIImage*)image;

-(NSArray*) getAllSavedArtwork;

-(BOOL) deleteArtwork:(NCArtwork*) artwork fromAuthor:(NCAuthor*) author;

//Tagd

-(NSDictionary*) getTagsDictionary;

-(void) getAllArtworksTagswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

//Authors

-(void)getAllAuthorswithCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

-(void)getAuthorWithID:(NSString*) string withCompletionBlock:(void (^)(NSError *, NCAuthor *))completionBlock;

-(void) setCurrentAuthor:(NCAuthor*) author;

-(NCAuthor*) currentAuthor;

//Works

-(void) getWorksOrderedByDateFrom:(int) firstWork to:(int) lastWork withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;


-(void) saveArraysToDocuments;

-(void) getAllArtworksOrderedByDateWithCompletionBlock:(void (^)(NSError* error, NSArray * artworks))completionBlock;

-(void) getWorkWithId: (NSString*) workID withCompletionBlock:(void (^)(NSError *, NCArtwork *))completionBlock;

-(void) publishArtWorkWithTitle:(NSString*)title withDescription:(NSString*)description withTags:(NSString*)tags withLocation:(CLLocationCoordinate2D) coordinate withImage:(UIImage*)image withCompletionBlock:(void (^)(NSError *, NSDictionary *))completionBlock;

-(void) getWorksWithIds: (NSArray *) arrayWithIds withCompletionBlock:(void (^)(NSError *, NSArray *))completionBlock;

//Authentication

-(void) validateAndCreateUser:(NSString *) fbUserId withCompletionBlock:(void (^)(NSError *, NSDictionary* userDictionary))completionBlock;

//Class Methods

+ (NCArtworkManager*) sharedManager;

//Saved Artworks

-(void) removeSavedArtwork:(UIImage*) image;



@end
