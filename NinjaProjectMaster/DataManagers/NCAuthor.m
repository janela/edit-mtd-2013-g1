//
//  NSAuthor.m
//  Ninja App Simplex 2
//
//  Created by Rui Bordadágua on 08/06/13.
//  Copyright (c) 2013 Rui Bordadágua. All rights reserved.
//

#import "NCAuthor.h"

@implementation NCAuthor

- (id)initFromDictionary:(NSDictionary*) dictionary
{
    self = [super init];
    if (self) {
        
        NSLog(@"id:%@", dictionary);
        
        self.identity = [[dictionary objectForKey:@"Id"] stringValue];
        
        self.facebookid = [dictionary objectForKey:@"FBID"];
        self.name = [dictionary objectForKey:@"Name"];
        self.obras = [dictionary objectForKey:@"Works"];
        
        
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder{
    
    
    [aCoder encodeObject:self.obras forKey:@"Works"];
    [aCoder encodeObject:self.facebookid forKey:@"FBID"];
    [aCoder encodeObject:self.name forKey:@"Name"];
    [aCoder encodeObject:self.identity forKey:@"Id"];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    NCAuthor * author = [[NCAuthor alloc]init];
    
    author.obras = [aDecoder decodeObjectForKey:@"Works"];
    author.facebookid = [aDecoder decodeObjectForKey:@"FBID"];
    author.identity = [aDecoder decodeObjectForKey:@"Id"];
    author.name = [aDecoder decodeObjectForKey:@"Name"];
    
    return author;
    
}

@end
