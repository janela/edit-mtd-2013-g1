//
//  EDArtworkManagerStubDataSource.h
//  EDITStub
//
//  Created by Tiago Janela on 20/05/13.
//  Copyright (c) 2013 Bliss Applications. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NCArtworkManagerDataSource.h"

@interface NCArtworkManagerStubDataSource : NSObject/**<NCArtworkManagerDataSource>*/

@end
