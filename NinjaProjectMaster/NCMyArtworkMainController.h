//
//  NinjaMinhasImagensViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NinjaBaseDetailViewController.h"
#import "NCImageCacheDelegate.h"

@interface NCMyArtworkMainController : NinjaBaseDetailViewController <UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,NCImageCacheDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *myArtworkCollection;

- (IBAction)LogoutButtonPressed:(id)sender;


@end
