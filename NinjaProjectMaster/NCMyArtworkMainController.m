//
//  NinjaMinhasImagensViewController.m
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import "NCMyArtworkMainController.h"
#import "NCMyArtworkCell.h"
#import "NCArtwork.h"
#import "NCArtworkManager.h"
#import "NCArtworkDetailViewController.h"
#import "NCCriarObraViewController.h"
#import <Parse/Parse.h>
#import "NinjaLoginViewController.h"
#import "NCHeaderCollectionView.h"
#import "NCImageCacheManager.h"

@interface NCMyArtworkMainController ()
{
    
    
    NSArray * publishedArtwork;
    
    NSArray * savedArtwork;
    
    NCAuthor * currentAuthor;
    
    BOOL allImagesdownloaded;
    
    
}


@end

@implementation NCMyArtworkMainController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ninjahome.jpg"]];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown) {
        //NSLog(@"PORTRAIT");
        
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
        
    }else{
        // NSLog(@"LANDCAPE");
        
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    currentAuthor = [[NCArtworkManager sharedManager] currentAuthor];
}

- (void)viewDidAppear:(BOOL)animated{
    
    savedArtwork = [[NCArtworkManager sharedManager] getAllSavedArtwork];

        
    [[NCArtworkManager sharedManager] getAllArtworksForUserId:currentAuthor.identity withCompletionBlock:^(NSError * error, NSArray * array){
        
            publishedArtwork = [[NSArray alloc]initWithArray:array];
        
            NSLog(@"%@",publishedArtwork);
        
        [[NCImageCacheManager sharedManager] ncImageCacheDelegate:self arrayWithArtworksForImageDownloading:publishedArtwork];
        
        
    }];
        
}


- (BOOL)shouldAutorotateToInterfaceOrientation:
(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return YES;
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation) fromInterfaceOrientation
{
    // Set UIWebView Background Image
    if (UIDeviceOrientationIsPortrait([UIDevice currentDevice].orientation))
    {
        // code for Portrait orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"portraitFundoDetail.jpg"]];
    }
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        // code for landscape orientation
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"landscapeFundoDetail.jpg"]];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Collection View DataSource Methods

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];

    
    if(indexPath.section ==0){
        
        NCCriarObraViewController * criarobra = (NCCriarObraViewController*)[storyboard instantiateViewControllerWithIdentifier:@"CreateArtwork"];
        
        [criarobra setInitialImage:[savedArtwork objectAtIndex:indexPath.row]];
        [[self navigationController]pushViewController:criarobra animated:YES];
        
        
    }
    
    else if(indexPath.section ==1){
        
        NCArtworkDetailViewController * detailview = (NCArtworkDetailViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ShowArtworkDetailView"];        detailview.artwork = [publishedArtwork objectAtIndex:indexPath.row];
        
        [[self navigationController] pushViewController:detailview animated:YES];
        
        
    }
    
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
     NCMyArtworkCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyArtworkCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    
    if(indexPath.section ==0){
        
        cell.imageView.image = [savedArtwork objectAtIndex:indexPath.row];
        
    }
    else if(indexPath.section ==1){
        
        
        NCArtwork * artwork = [publishedArtwork objectAtIndex:indexPath.row];
        cell.artwork = artwork;
        
        
        cell.imageView.image = cell.artwork.image;

        
    }
    return cell;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    int count=0;
    
    if(section==0){
        
        count = [savedArtwork count];
    }
    else if (section ==1){
     
        count = [publishedArtwork count];
    }

    return count;

}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 2;
    
}
#pragma mark – UICollectionViewDelegateFlowLayout

// 1
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    //NCArtwork * artwork = [self.searchResults objectAtIndex:indexPath.row];
    // 2
    CGSize retval = /**artwork..thumbnail.size.width > 0 ? photo.thumbnail.size : */CGSizeMake(100, 100);
    retval.height += 35; retval.width += 35; return retval;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    NCHeaderCollectionView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SectionHeader" forIndexPath:indexPath];
    
    if (indexPath.section == 0) {
        headerView.imageHeaderSection.image = [UIImage imageNamed:@"tituloImagensGuardadas.png"];
        
    }else{
        headerView.imageHeaderSection.image = [UIImage imageNamed:@"tituloImagensPublicadas.png"];
    }
    return headerView;
    
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [PFUser logOut];
    
    // Return to login view controller
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    NinjaLoginViewController *launchViewController = (NinjaLoginViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self presentViewController:launchViewController animated:YES completion:NULL];
    
    
    NSLog(@"Logged Out!");
}

-(void)allArtworkImagesDownloaded{
    
    [self.myArtworkCollection reloadData];
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView.contentOffset.y == scrollView.contentSize.height - scrollView.frame.size.height) {
        
        NSLog(@"Cheguei ao fim");
        // reached the bottom
    }
}

@end
