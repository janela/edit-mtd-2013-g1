//
//  NinjaBaseDetailViewController.h
//  NinjaProjectMaster
//
//  Created by Luis Custodio on 5/28/13.
//  Copyright (c) 2013 Luis Custodio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NinjaBaseDetailViewController : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate>

@property (strong, nonatomic) UIPopoverController *mainPopoverController;

@end
